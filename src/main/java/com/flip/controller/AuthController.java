/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.controller;

import com.flip.domain.FileWrapper;
import com.flip.domain.OperationResponse;
import com.flip.domain.User;
import com.flip.enums.ResultCode;
import com.flip.enums.SecurityConstants;
import com.flip.enums.SqlConstants;
import com.flip.form.CheckForm;
import com.flip.form.InfoForm;
import com.flip.form.OperationCodeForm;
import com.flip.service.FtpService;
import com.flip.service.AuthService;
import com.flip.util.Crypto;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URL;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author ahmadov
 */
@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins = "*")
public class AuthController {
    private static final Logger log = Logger.getLogger(AuthController.class);
    
    @Value("${ftp.root.directory}")
    protected String rootDirectory;
    
    @Autowired
    private AuthService service;
    
    @Autowired
    private FtpService ftpService;
    
    @Autowired
    private CacheManager cacheManager;
    
    @PostMapping(value = "/auth/{app}/{controller}/{method}", produces = MediaType.APPLICATION_JSON_VALUE)
    protected ResponseEntity forwardApiPost(@PathVariable String app,
                                           @PathVariable String controller,
                                           @PathVariable String method,
                                           HttpServletRequest request,
                                           HttpServletResponse response) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            URL url1 = new URL(request.getRequestURL().toString());

            String dbName = "flip";
            String requestData = request.getReader().lines().collect(Collectors.joining());
            String ssid = request.getHeader("Auth").substring(5);
            String privilegeUrl = "/auth/" + app + "/" + controller + "/" + method;
            int count = 0;
            
            OperationResponse userOperation = this.getCacheOperation(ssid);
            
//            String url = "http://localhost:8080/"+ app +"Rest/local";
            String url = "http://flipsystemrest:8080/local";
//            String url = "http://173.212.212.209:8182/"+ app +"Rest/local";
            
            if(userOperation.getCode() != ResultCode.OK) {
               return new ResponseEntity<String>("[\"Unauthorized: ssid is invalid\"]", HttpStatus.UNAUTHORIZED);
            }
            
            User user = (User) userOperation.getData();
            
            for(String s: user.getPrivilegeUrlList()) {
                if(privilegeUrl.equals(s)) {
                    ++count;
                    break;
                }
            }
            
            if(count == 0) {
               return new ResponseEntity<String>("[\"Unauthorized: User access denied!!!\"]", HttpStatus.UNAUTHORIZED);
            }
            
            
            RestTemplate template = new RestTemplate();
            method = method.substring(0,1).toLowerCase() + method.substring(1);
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
            builder.queryParam("json", requestData);
            builder.queryParam("ctrl", controller);
            builder.queryParam("method", method);
            builder.queryParam("userId", user.getId());
            builder.queryParam("userType", user.getUserType() != null ? user.getUserType() : "");
//            builder.queryParam("jsonType", "SIMPLE");
            ResponseEntity<OperationResponse> responseEntity = template.exchange(builder.build().encode().toUri(),
                                                                                    HttpMethod.POST,
                                                                                    null, 
                                                                                    OperationResponse.class);
            operationResponse = responseEntity.getBody();
            
            if(HttpMethod.OPTIONS.name().equals(request.getMethod())) {
                return ResponseEntity.ok(operationResponse.getCode());
            }
            
            if(operationResponse.getCode() != ResultCode.OK) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
            }
            
            if(controller.toUpperCase().equals("USERS") && (method.toUpperCase().equals("EDIT") || 
                                                            method.toUpperCase().equals("DELETE"))) {
                removeAllCache();
            }
            
            if(controller.toUpperCase().equals("TRANSLATES") && (method.toUpperCase().equals("ADD") || 
                                                            method.toUpperCase().equals("EDIT") || 
                                                            method.toUpperCase().equals("DELETE"))) {
                changeMultilangDirectory();
                
            }
            
            if(SecurityConstants.checkUrlForChangeCoin(controller + "/" + method)) {
                String coin = service.getUserCoin(user.getId());
                Cache cache = cacheManager.getInstance().getCache("cache");
                user.setFlipCoin(coin);
                cache.putIfAbsent(new Element(ssid, user));
            }
            
        } 
        catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return ResponseEntity.ok(operationResponse.getData());
    }
    
    @PostMapping(value = "/common/{app}/{controller}/{method}", produces = MediaType.APPLICATION_JSON_VALUE)
    protected ResponseEntity forwardApiGet(@PathVariable String app,
                                           @PathVariable String controller,
                                           @PathVariable String method,
                                           HttpServletRequest request,
                                           HttpServletResponse response) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            URL url1 = new URL(request.getRequestURL().toString());
            
            String requestData = request.getReader().lines().collect(Collectors.joining());
            String privilegeUrl = "/common/" + app + "/" + controller + "/" + method;
//            int count = 0;
            
            
//            String url = "http://localhost:8080/"+ app +"Rest/local";
            String url = "http://flipsystemrest:8080/local";
//            String url = "http://173.212.212.209:8182/"+ app +"Rest/local";
            
            
            if(!SecurityConstants.checkPrivilegeUrl(privilegeUrl)) {
               return new ResponseEntity<String>("[\"Unauthorized: User access denied!!!\"]", HttpStatus.UNAUTHORIZED);
            }
            
            RestTemplate template = new RestTemplate();
            method = method.substring(0,1).toLowerCase() + method.substring(1);
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
            builder.queryParam("json", requestData);
            builder.queryParam("ctrl", controller);
            builder.queryParam("method", method);
            builder.queryParam("userId", "0");
            builder.queryParam("userType", "COMMON");
//            builder.queryParam("jsonType", "SIMPLE");
            ResponseEntity<OperationResponse> responseEntity = template.exchange(builder.build().encode().toUri(),
                                                                                    HttpMethod.POST,
                                                                                    null, 
                                                                                    OperationResponse.class);
            operationResponse = responseEntity.getBody();
            
            if(HttpMethod.OPTIONS.name().equals(request.getMethod())) {
                return ResponseEntity.ok(operationResponse.getCode());
            }
            
            if(operationResponse.getCode() != ResultCode.OK) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
            }
            
        } 
        catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return ResponseEntity.ok(operationResponse.getData());
    }
    
    
    protected OperationResponse addCacheOperation(String token, User user) {
    OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            Cache cache = cacheManager.getInstance().getCache("cache");
            
                cache.putIfAbsent(new Element(token, user));
            
        } 
        catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    protected OperationResponse checkToken(@PathVariable String ssid, HttpServletRequest httpServletRequest) {
    OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            Cache cache = cacheManager.getInstance().getCache("cache");
                operationResponse = service.checkToken(ssid);
                User user = (User) operationResponse.getData();
//                URL url1 = new URL(httpServletRequest.getRequestURL().toString());
//                String host1 =  url1.getHost().equals("localhost") ? "localhost:8080" : url1.getHost();
//                List<Application> appList = user.getApplications();
//
//                for (Application app : appList) {
//                        app.setUrl(app.getUrl().replace(app.getUrl().split("//")[1].split("/")[0], host1));
//                }
                if(operationResponse.getCode() == ResultCode.OK) 
                    cache.putIfAbsent(new Element(ssid, user));
        } 
        catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    protected OperationResponse getCacheOperation(String token) {
    OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            Cache cache = cacheManager.getInstance().getCache("cache");
            String userId = "";
            if(!cache.isKeyInCache(token)) {
                operationResponse = service.checkToken(token.split("\\.")[2]);
                User user = (User) operationResponse.getData();
//                List<Application> appList = user.getApplications();

//                for (Application app : appList) {
//                        app.setUrl(app.getUrl().replace(app.getUrl().split("//")[1].split("/")[0], host));
//                }
                if(operationResponse.getCode() == ResultCode.OK) {
                    cache.putIfAbsent(new Element(token, user));
                    operationResponse.setData(cache.get(token).getObjectValue());
                    operationResponse.setCode(ResultCode.OK);
                }
                userId = user.getId();
                
            } else {
                
                if(cache.get(token) != null && cache.get(token).getObjectValue() != null) {
                    operationResponse.setData(cache.get(token).getObjectValue());
                    operationResponse.setCode(ResultCode.OK);
//                    userId = ((User) cache.get(token).getObjectValue()).getId();
                } else {
                    operationResponse = service.checkToken(token.split("\\.")[2]);
                    User user = (User) operationResponse.getData();
//                    List<Application> appList = user.getApplications();
//
//                    for (Application app : appList) {
//                            app.setUrl(app.getUrl().replace(app.getUrl().split("//")[1].split("/")[0], host));
//                    }
                    if(operationResponse.getCode() == ResultCode.OK) {
                        cache.putIfAbsent(new Element(token, user));
                        operationResponse.setData(cache.get(token).getObjectValue());
                        operationResponse.setCode(ResultCode.OK);
                    }
//                    userId = user.getId();
                }
            }
            
//            service.changeUserLastAction(userId, domain);
        } 
        catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @GetMapping(value = "/addCache/{ssid}")
    protected OperationResponse addCache(@PathVariable String ssid) {
    OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            Cache cache = cacheManager.getInstance().getCache("cache");
            
            operationResponse = service.checkToken(ssid);
            if(operationResponse.getCode() == ResultCode.OK)
                cache.putIfAbsent(new Element(ssid, operationResponse.getData()));
            
        } 
        catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @GetMapping(value = "/removeCache/{ssid}")
    protected OperationResponse removeCache(@PathVariable String ssid) {
    OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            Cache cache = cacheManager.getInstance().getCache("cache");
            cache.remove(ssid);
            cache.removeAll();
            
        } 
        catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    protected OperationResponse removeAllCache() {
    OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            Cache cache = cacheManager.getInstance().getCache("cache");
            cache.removeAll();
            operationResponse.setCode(ResultCode.OK);
        } 
        catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    protected OperationResponse removeCacheOperation(String token) {
    OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            Cache cache = cacheManager.getInstance().getCache("cache");
            cache.remove(token);
            
        } 
        catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @PostMapping(value = "/auth/uploadFile", consumes = MediaType.MULTIPART_FORM_DATA_VALUE) 
    protected OperationResponse uploadAuthFiles(@RequestPart(name = "image", required = false) MultipartFile image,
                                            HttpServletRequest request) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
                String token = request.getHeader("Auth").substring(5);
            
                URL url1 = new URL(request.getRequestURL().toString());
                OperationResponse userOperationResponse = this.getCacheOperation(token);
                
                if(userOperationResponse.getCode() != ResultCode.OK) {
                    operationResponse.setCode(ResultCode.UNAUTHORIZED);
                   return operationResponse;
                }

                User user = (User) userOperationResponse.getData();
            
            
            if (image != null && image.getSize() > 0) {
                String directory = rootDirectory + "/" + user.getEmail();
                OperationResponse uploadResponse = ftpService.saveFtpFile(directory, image, Crypto.getGuid());
                if(uploadResponse.getCode() != ResultCode.OK) {
                    throw new Exception("Error upload file");
                }
                
                operationResponse = service.uploadFileAndGetId(image.getOriginalFilename(), 
                                                                image.getContentType(), 
                                                                (String) uploadResponse.getData(),
                                                                image.getSize(), 
                                                                user.getId());
                
            } else {
                throw new Exception("Empty file");
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            
        }
        return operationResponse;
    }
    
    @PostMapping(value = "/uploadFile", consumes = MediaType.MULTIPART_FORM_DATA_VALUE) 
    protected OperationResponse uploadFiles(@RequestPart(name = "image", required = false) MultipartFile image,
                                            HttpServletRequest request) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            String token = request.getHeader("Auth");
            token = token != null && !token.trim().isEmpty() ? token.substring(5) : "";
            String userEmail = "COMMON";
            String userId = "0";
            if(!token.trim().isEmpty()) {
                URL url1 = new URL(request.getRequestURL().toString());
                OperationResponse userOperationResponse = this.getCacheOperation(token);
    //            
    //            
                if(userOperationResponse.getCode() != ResultCode.OK) {
                    operationResponse.setCode(ResultCode.UNAUTHORIZED);
                   return operationResponse;
                }

                User user = (User) userOperationResponse.getData();
                userEmail = user.getEmail();
                userId = user.getId();
            }
            
            
            if (image != null && image.getSize() > 0) {
                String directory = rootDirectory + "/" + userEmail;
                OperationResponse uploadResponse = ftpService.saveFtpFile(directory, image, Crypto.getGuid());
                if(uploadResponse.getCode() != ResultCode.OK) {
                    throw new Exception("Error upload file");
                }
                
                operationResponse = service.uploadFileAndGetId(image.getOriginalFilename(), 
                                                                image.getContentType(), 
                                                                (String) uploadResponse.getData(),
                                                                image.getSize(), 
                                                                userId);
                
            } else {
                throw new Exception("Empty file");
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            
        }
        return operationResponse;
    }
    
    @PostMapping(value = "/auth/file/{id:\\d+}/remove")
    protected OperationResponse removeFile(@PathVariable long id,
                                            HttpServletRequest request) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            
            String token = request.getHeader("Auth").substring(5);
            URL url1 = new URL(request.getRequestURL().toString());
            OperationResponse userOperationResponse = this.getCacheOperation(token);
//            
            User user = (User) userOperationResponse.getData();
            if(userOperationResponse.getCode() != ResultCode.OK) {
                operationResponse.setCode(ResultCode.UNAUTHORIZED);
               return operationResponse;
            }
            
            
            operationResponse = service.getFileByIdAndCheckUser(String.valueOf(id), user.getId());
            
            if (operationResponse.getCode().equals(ResultCode.OK) && operationResponse.getData() != null) {
                FileWrapper file = (FileWrapper) operationResponse.getData();
                OperationResponse removeResponse = ftpService.removeFtpFile(file.getPath());
                if(removeResponse.getCode() != ResultCode.OK) {
                    throw new Exception("Error remove file");
                }
                
                operationResponse = service.removeFile(String.valueOf(id), user.getId());
                
            } else {
                throw new Exception("Invalid file Id");
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            
        }
        return operationResponse;
    }
    
    @PostMapping(value = "/auth/profile/image/remove")
    protected OperationResponse removeProfileImageFile(HttpServletRequest request) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            
            String ssid = request.getHeader("Auth").substring(5);
            URL url1 = new URL(request.getRequestURL().toString());
            OperationResponse userOperationResponse = this.getCacheOperation(ssid);
//            
            User user = (User) userOperationResponse.getData();
            if(userOperationResponse.getCode() != ResultCode.OK) {
                operationResponse.setCode(ResultCode.UNAUTHORIZED);
               return operationResponse;
            }
            
            operationResponse = service.getFileByIdAndCheckUser(user.getPhotoFileId(), user.getId());
            
            if (operationResponse.getCode().equals(ResultCode.OK) && operationResponse.getData() != null) {
                
                FileWrapper file = (FileWrapper) operationResponse.getData();
                
                OperationResponse removeResponse = ftpService.removeFtpFile(file.getPath());
                if(removeResponse.getCode() != ResultCode.OK) {
                    throw new Exception("Error remove file");
                }
                
                operationResponse = service.removeFile(file.getId(), user.getId());
                
            } else {
                throw new Exception("Invalid file Id");
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            
        }
        return operationResponse;
    }
    
    @GetMapping(value = "/get/file/{id:\\d+}", produces = MediaType.IMAGE_JPEG_VALUE)
    protected ResponseEntity<byte[]> getFile(@PathVariable long id,
                                @RequestParam(defaultValue = "") String fileType,
                                @RequestParam(defaultValue = "") String size,
                                HttpServletRequest request) {
        try {
            
            URL url1 = new URL(request.getRequestURL().toString());
            OperationResponse operationResponse = service.getFileById(String.valueOf(id));
            FileWrapper file = (FileWrapper) operationResponse.getData();
            if (file != null && Long.valueOf(file.getId()) > 0) {
                HttpHeaders headers = new HttpHeaders();
                    String contentType = file.getContentType() != null && !file.getContentType().trim().isEmpty() ? file.getContentType().trim() : "image/jpeg";
                    headers.setContentType(MediaType.parseMediaType(contentType));
                    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
                    if(!size.trim().isEmpty() && size.split("x").length == 2) {
                        ResponseEntity<byte[]> response = new ResponseEntity<>(ftpService.downloadFtpFileWithSize(file.getPath(), size), headers, HttpStatus.OK);
                        return response;
                    } else {
                        ResponseEntity<byte[]> response = new ResponseEntity<>(ftpService.downloadFtpFile(file.getPath()), headers, HttpStatus.OK);
                        return response;
                    }
                    
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
    
    private void changeMultilangDirectory() {
        File dir = new File("/usr/local/tomcat9/webapps/TRANSLATE");
        File dir2 = new File("/usr/local/tomcat9/TRANSLATE");
        File dir3 = new File("/usr/local/tomcat9/webapps/edutube/TRANSLATE");
        File dir4 = new File("/usr/local/tomcat9/webapps/meetadore/TRANSLATE");
        File dir5 = new File("/usr/local/tomcat9/webapps/admission/TRANSLATE");
        File dir6 = new File("/usr/local/tomcat9/webapps/admission/admission/TRANSLATE");
        File dir7 = new File("/usr/local/tomcat9/webapps/meetadore/meet/TRANSLATE");
        String queryAz = "{\n";
        String queryEn = "{\n";
        String queryRu = "{\n";
        if(!dir.exists()) {
            dir.mkdir();
        }
        if(!dir2.exists()) {
            dir2.mkdir();
        }
        
        if(!dir3.exists()) {
            dir3.mkdir();
        }
        
        if(!dir4.exists()) {
            dir4.mkdir();
        }
        
        if(!dir5.exists()) {
            dir5.mkdir();
        }
        
        if(!dir6.exists()) {
            dir6.mkdir();
        }
        
        if(!dir7.exists()) {
            dir7.mkdir();
        }
        
        File az = new File(dir, "az.json");
        File en = new File(dir, "en.json");
        File ru = new File(dir, "ru.json");
        
        File az2 = new File(dir2, "az.json");
        File en2 = new File(dir2, "en.json");
        File ru2 = new File(dir2, "ru.json");
        
        File az3 = new File(dir3, "az.json");
        File en3 = new File(dir3, "en.json");
        File ru3 = new File(dir3, "ru.json");
        
        File az4 = new File(dir4, "az.json");
        File en4 = new File(dir4, "en.json");
        File ru4 = new File(dir4, "ru.json");
        
        File az5 = new File(dir5, "az.json");
        File en5 = new File(dir5, "en.json");
        File ru5 = new File(dir5, "ru.json");
        
        File az6 = new File(dir6, "az.json");
        File en6 = new File(dir6, "en.json");
        File ru6 = new File(dir6, "ru.json");
        
        File az7 = new File(dir7, "az.json");
        File en7 = new File(dir7, "en.json");
        File ru7 = new File(dir7, "ru.json");
        
//        for(DictionaryWrapper d: service.getTranslateList()) {
//            String nameAz = d.getValue().getAz() != null ? d.getValue().getAz().trim() : "";
//            String nameEn = d.getValue().getEn() != null ? d.getValue().getEn().trim() : "";
//            String nameRu = d.getValue().getRu() != null ? d.getValue().getRu().trim() : "";
//            queryAz += "\"" + d.getId() + "\" : \"" + nameAz +"\",\n"; 
//            queryEn += "\"" + d.getId() + "\" : \"" + nameEn +"\",\n"; 
//            queryRu += "\"" + d.getId() + "\" : \"" + nameRu +"\",\n"; 
//        }
        if(queryAz.length() > 5) 
            queryAz = queryAz.substring(0, queryAz.trim().length() -1) + "\n }";
        else 
            queryAz += "}";
        
        if(queryEn.length() > 5) 
            queryEn = queryEn.substring(0, queryEn.trim().length() -1) + "\n }";
        else 
            queryEn += "}";
        
        if(queryRu.length() > 5) 
            queryRu = queryRu.substring(0, queryRu.trim().length() -1) + "\n }";
        else 
            queryRu += "}";
        
        try (Writer writer1 = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(az), "utf-8"));
            Writer writer2 = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(en), "utf-8"));
            Writer writer3 = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(ru), "utf-8"));
            Writer writer4 = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(az2), "utf-8"));
            Writer writer5 = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(en2), "utf-8"));
            Writer writer6 = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(ru2), "utf-8"));
            Writer writer7 = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(az3), "utf-8"));
            Writer writer8 = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(en3), "utf-8"));
            Writer writer9 = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(ru3), "utf-8"));
            Writer writer10 = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(az4), "utf-8"));
            Writer writer11 = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(en4), "utf-8"));
            Writer writer12 = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(ru4), "utf-8"));
            Writer writer13 = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(az5), "utf-8"));
            Writer writer14 = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(en5), "utf-8"));
            Writer writer15 = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(ru5), "utf-8"));
            Writer writer16 = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(az6), "utf-8"));
            Writer writer17 = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(en6), "utf-8"));
            Writer writer18 = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(ru6), "utf-8"));
            Writer writer19 = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(az7), "utf-8"));
            Writer writer20 = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(en7), "utf-8"));
            Writer writer21 = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(ru7), "utf-8"))) {

              writer1.write(queryAz);
              writer2.write(queryEn);
              writer3.write(queryRu);
              
              writer4.write(queryAz);
              writer5.write(queryEn);
              writer6.write(queryRu);
              
              writer7.write(queryAz);
              writer8.write(queryEn);
              writer9.write(queryRu);
              
              writer10.write(queryAz);
              writer11.write(queryEn);
              writer12.write(queryRu);
              
              writer13.write(queryAz);
              writer14.write(queryEn);
              writer15.write(queryRu);
              
              writer16.write(queryAz);
              writer17.write(queryEn);
              writer18.write(queryRu);
              
              writer19.write(queryAz);
              writer20.write(queryEn);
              writer21.write(queryRu);
        } catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        
    }
    
    @PostMapping(value = "/auth/change/info")
    protected OperationResponse changeInfo(@RequestBody InfoForm form,
                                          HttpServletResponse httpServletResponse,
                                          HttpServletRequest request) {
        
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            Cache cache = cacheManager.getInstance().getCache("cache");
            String ssid = request.getHeader("Auth").substring(5);
            
            URL url1 = new URL(request.getRequestURL().toString());
            OperationResponse userOperationResponse = this.getCacheOperation(ssid);
            
            User user = (User) userOperationResponse.getData();
            if(userOperationResponse.getCode() != ResultCode.OK) {
                operationResponse.setCode(ResultCode.UNAUTHORIZED);
                
               return operationResponse;
            }
            
            operationResponse = service.changeUserInfo(form, user.getId(), "INFO");
            
            if(operationResponse.getCode().equals(ResultCode.OK)) {
                user.setEmail(form.getEmail());
                user.setFullname(form.getFullname());
                user.setPhone(form.getPhone());
                cache.putIfAbsent(new Element(ssid, user));
            }
            
            
        } 
        catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @PostMapping(value = "/auth/change/password")
    protected OperationResponse changePassword(@RequestBody InfoForm form,
                                          HttpServletResponse httpServletResponse,
                                          HttpServletRequest request) {
        
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            Cache cache = cacheManager.getInstance().getCache("cache");
            String ssid = request.getHeader("Auth").substring(5);
            
            URL url1 = new URL(request.getRequestURL().toString());
            OperationResponse userOperationResponse = this.getCacheOperation(ssid);
            
            User user = (User) userOperationResponse.getData();
            if(userOperationResponse.getCode() != ResultCode.OK) {
                operationResponse.setCode(ResultCode.UNAUTHORIZED);
                
               return operationResponse;
            }
            
            operationResponse = service.changeUserInfo(form, user.getId(), "PASSWORD");
            
            
            
            
        } 
        catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @PostMapping(value = "/auth/change/profile/image")
    protected OperationResponse changeProfileImage(@RequestBody InfoForm form,
                                          HttpServletResponse httpServletResponse,
                                          HttpServletRequest request) {
        
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            Cache cache = cacheManager.getInstance().getCache("cache");
            String ssid = request.getHeader("Auth").substring(5);
            
            URL url1 = new URL(request.getRequestURL().toString());
            OperationResponse userOperationResponse = this.getCacheOperation(ssid);
            
            User user = (User) userOperationResponse.getData();
            if(userOperationResponse.getCode() != ResultCode.OK) {
                operationResponse.setCode(ResultCode.UNAUTHORIZED);
                
               return operationResponse;
            }
            String photoFileId = user.getPhotoFileId();
            if(photoFileId != null && !photoFileId.trim().isEmpty() && !photoFileId.equals("0")) {
                operationResponse = service.getFileByIdAndCheckUser(user.getPhotoFileId(), user.getId());
                if (operationResponse.getCode().equals(ResultCode.OK) && operationResponse.getData() != null) {
                
                    FileWrapper file = (FileWrapper) operationResponse.getData();

                    OperationResponse removeResponse = ftpService.removeFtpFile(file.getPath());
//                    if(removeResponse.getCode() != ResultCode.OK) {
//                        throw new Exception("Error remove file");
//                    }

                    operationResponse = service.removeFile(file.getId(), user.getId());

                }
            }
            
            operationResponse = service.changeUserInfo(form, user.getId(), "PHOTO");
            if(operationResponse.getCode().equals(ResultCode.OK)) {
                user.setPhotoFileId(form.getPhotoFileId());
                cache.putIfAbsent(new Element(ssid, user));
            }
            
            
            
        } 
        catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @PostMapping("/auth/user/check")
    protected OperationResponse checkUser(HttpServletResponse httpServletResponse,
                                        HttpServletRequest request) {
        
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            
            String token = request.getHeader("Auth").substring(5);
            URL url1 = new URL(request.getRequestURL().toString());
            OperationResponse userOperationResponse = this.getCacheOperation(token);
//            
            User user = (User) userOperationResponse.getData();
            if(userOperationResponse.getCode() != ResultCode.OK) {
                operationResponse.setCode(ResultCode.UNAUTHORIZED);
                
               return operationResponse;
            }
            
            
            operationResponse.setData(user);
            operationResponse.setCode(ResultCode.OK);
            
        } 
        catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @PostMapping("/check/data")
    protected OperationResponse checkData(@RequestBody CheckForm form) {
        
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            
            return service.checkEmailOrPhone(form);
            
        } 
        catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @PostMapping("/check/operationCode")
    protected OperationResponse checkData(@RequestBody OperationCodeForm form) {
        
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            
            return service.checkAnnouncementOperationCode(form);
            
        } 
        catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

}
