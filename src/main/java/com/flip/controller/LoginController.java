/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.controller;

import com.flip.domain.UserProperties;
import com.flip.domain.MultilanguageString;
import com.flip.domain.OperationResponse;
import com.flip.domain.User;
import com.flip.enums.ResultCode;
import com.flip.form.RegisterForm;
import com.flip.form.UserForm;
import com.flip.security.JwtGenerator;
import com.flip.service.AuthService;
import com.flip.service.FtpService;
import com.flip.util.Crypto;
import com.flip.validator.LoginValidator;
import com.flip.validator.RegisterValidator;
import java.io.InputStream;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import javax.net.ssl.SSLContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author ahmadov
 */
@RestController
@CrossOrigin(origins = "*", exposedHeaders = "Auth")
public class LoginController {
    private static final Logger log = Logger.getLogger(AuthController.class);
    
    @Value("${ftp.root.directory}")
    protected String rootDirectory;
    
    @Autowired
    private JwtGenerator jwtGenerator;
    
    @Autowired
    private AuthService service;
    
    @Autowired
    private FtpService ftpService;
    
    @PostMapping("/login")
    public OperationResponse login(@RequestBody final UserProperties userProperties,
                                    HttpServletResponse httpServletResponse,
                                    HttpServletRequest httpServletRequest,
                                    BindingResult result) {
        
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            URL url1 = new URL(httpServletRequest.getRequestURL().toString());
            LoginValidator validator = new LoginValidator(false);
            validator.validate(userProperties, result);
            
            if (result.hasErrors()) {
                operationResponse.setData(result.getAllErrors());
                operationResponse.setCode(ResultCode.INVALID_PARAMS);
                operationResponse.setMessage(new MultilanguageString("Parameterlərdə səhvlik var", 
                                                                     "Invalid parameters", 
                                                                     "Invalid parameters"));
                throw new Exception("Invalid params!!!");
            }
//            if(userProperties.getUsername() == null || userProperties.getUsername().trim().isEmpty() ||
//                    userProperties.getPassword() == null || userProperties.getPassword().trim().isEmpty())
//                return operationResponse;
            
            OperationResponse response = service.doLogin(userProperties);

            User user = (User) response.getData();
            
            if (response.getCode() == ResultCode.ERROR && (response.getMessage() != null) && response.getMessage().getAz().equals("INVPARAMS")) {
                operationResponse.setMessage(new MultilanguageString("İstifadəçi adı və ya şifrə yanlışdır!", 
                                                                     "Invalid username/password!",  
                                                                     "Hеправильное имя пользователя или пароль!"));
                throw new Exception("Invalid username/password. Username: " + userProperties.getUsername());
            }  else if (response.getCode() == ResultCode.ERROR) {
                operationResponse.setMessage(new MultilanguageString("Əlaqədə problem var!", 
                                                                     "Problem with connection!",  
                                                                     "Проблема с подключением!"));
                throw new Exception("Problem with connection");
            }
            
            userProperties.setId(Long.valueOf(user.getId()));
            userProperties.setRole(user.getEmail());
            String token = jwtGenerator.generate(userProperties);
            OperationResponse newApplicationSessionResponse = service.addUserSession(Long.valueOf(user.getId()), token.split("\\.")[2]);
            
            if (newApplicationSessionResponse.getCode() != ResultCode.OK) {
                operationResponse.setMessage(new MultilanguageString("Sessiya yaranmadi!", 
                                                                     "Session is not create!",  
                                                                     "Сессия не создает!"));
                throw new Exception("Session is not create" + userProperties.getUsername());
            }
            
            new AuthController().addCacheOperation(token, user);
            
            httpServletResponse.addHeader("Auth", token);
            
            operationResponse.setCode(ResultCode.OK);
            operationResponse.setData(user);
            
        } 
        catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @PostMapping("/system/logout")
    public ResponseEntity doLogout(HttpServletRequest request,
                            HttpServletResponse response) {
        
        String token = request.getHeader("Auth").substring(5);
        try {
            URL url1 = new URL(request.getRequestURL().toString());
           
            OperationResponse operationResponse = service.doExit(token.split("\\.")[2]);
            new AuthController().removeCacheOperation(token);
            return ResponseEntity.ok(operationResponse);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND);
    }
    
    
    @PostMapping(value = "/register/type/{type}/{accessToken}")
    protected OperationResponse loginWithFacebook(@PathVariable String accessToken,
                                                   @PathVariable String type,
                                                   HttpServletRequest request,
                                                   HttpServletResponse httpServletResponse) {

        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
            
        final String fields = "id,email,first_name,last_name,picture";
        try {
            
            
            TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
            SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
                    .loadTrustMaterial(null, acceptingTrustStrategy)
                    .build();
            SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
            CloseableHttpClient httpClient = HttpClients.custom()
                    .setSSLSocketFactory(csf)
                    .build();
            HttpComponentsClientHttpRequestFactory requestFactory =
                    new HttpComponentsClientHttpRequestFactory();
            requestFactory.setHttpClient(httpClient);

            RestTemplate restTemplate = new RestTemplate(requestFactory);
            
            
            URL url1 = new URL(request.getRequestURL().toString());
            String host1 =  url1.getHost().equals("localhost") ? "localhost:8080" : 
                               url1.getHost().equals("172.16.208.107") || 
                              !url1.getHost().equals(url1.getHost().replace("empro", "")) 
                                ? "172.16.208.107:8080" : 
                               url1.getHost().equals("192.168.0.118") ? "192.168.0.118:8080" : url1.getHost();
            String domain = url1.getHost().split("\\.").length > 2 && 
                            !url1.getHost().split("\\.")[0].equals("www") ?
                            url1.getHost().split("\\.")[0].toLowerCase() : "empro" ;
            String dbName = domain.equals("empro") ? "empro_emsdb" : domain.equals("demo") ? "emsdb" : domain + "_emsdb";
            
            
            System.out.println(accessToken);
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

            UriComponentsBuilder uriBuilder = type.equals("GMAIL") ? 
                    UriComponentsBuilder.fromUriString("https://www.googleapis.com/oauth2/v3/tokeninfo").queryParam("id_token", accessToken)
            : UriComponentsBuilder.fromUriString("https://graph.facebook.com/me").queryParam("access_token", accessToken).queryParam("fields", fields);

            String account = restTemplate.getForObject(uriBuilder.toUriString(), String.class);
//            System.out.println("$$$$$$$$$$$$" + account);
            JSONObject jsonObject = new JSONObject(account);
            String pictureUrl = "";
            if(jsonObject.get("email") == null || jsonObject.get("email").toString() == null || jsonObject.get("email").toString().trim().isEmpty()) {
                operationResponse.setCode(ResultCode.INVALID_PARAMS);
                operationResponse.setMessage(new MultilanguageString("Facebook email tapılmadı", "Facebook email not found", "Facebook email not found"));
                return operationResponse;
            }
            
            boolean userDetail = service.checkUserDetailsByEmail(jsonObject.get("email").toString().trim());
            
            if(!userDetail) {
                UserForm userForm = new UserForm();
                userForm.setEmail(jsonObject.get("email").toString());
                userForm.setName(type.equals("GMAIL") ? jsonObject.get("given_name").toString().trim() : jsonObject.get("first_name").toString().trim());
                userForm.setSurname(type.equals("GMAIL") ? jsonObject.get("family_name").toString().trim() : jsonObject.get("last_name").toString().trim());
                userForm.setId(type.equals("GMAIL") ? jsonObject.get("sub").toString() : jsonObject.get("id").toString());
                String fileName = "";
                String filePath = "";
                if(type.equals("FACEBOOK") && jsonObject.optJSONObject("picture") != null && jsonObject.getJSONObject("picture").optJSONObject("data") != null) {
                    pictureUrl = "https://graph.facebook.com/"+jsonObject.get("id").toString()+"/picture?type=large";
//                    jsonObject.getJSONObject("picture").getJSONObject("data").get("url").toString();
    //                if(!pictureUrl.trim().isEmpty()) {
                        URL url = new URL(pictureUrl);
                        InputStream is = url.openStream();
                        String directory = rootDirectory + "/profile";
                        OperationResponse uploadResponse = ftpService.saveFtpFile2(directory, is, Crypto.getGuid());
                        userForm.setFileName(userForm.getName());
                        userForm.setFilePath((String) uploadResponse.getData());
    //                }
                } else if(type.equals("GMAIL") && jsonObject.get("picture") != null && !jsonObject.get("picture").toString().trim().isEmpty()) {
                    pictureUrl = jsonObject.get("picture").toString() + "?sz=400";
//                    jsonObject.getJSONObject("picture").getJSONObject("data").get("url").toString();
    //                if(!pictureUrl.trim().isEmpty()) {
                        URL url = new URL(pictureUrl);
                        InputStream is = url.openStream();
                        String directory = rootDirectory + "/profile";
                        OperationResponse uploadResponse = ftpService.saveFtpFile2(directory, is, Crypto.getGuid());
                        userForm.setFileName(userForm.getName());
                        userForm.setFilePath((String) uploadResponse.getData());
    //                }
                }

//                operationResponse = service.registerWithOther(userForm, "", type.equals("GMAIL") ? "2" : "1", dbName);

                if(!operationResponse.getCode().equals(ResultCode.OK)) {
                    operationResponse.setMessage(new MultilanguageString("Əlaqədə problem var!", 
                                                                             "Problem with connection!",  
                                                                             "Проблема с подключением!"));
                    throw new Exception("Problem with connection");
                }
            }
            
            
            UserProperties userProperties = new UserProperties();
            userProperties.setUsername(jsonObject.get("email").toString().trim());
//            OperationResponse response = service.doLoginForDefaultUser(userProperties, dbName, "1");

//            User user = (User) response.getData();
    //            URL url1 = new URL(httpServletRequest.getRequestURL().toString());
    //            String host1 =  url1.getHost().equals("localhost") ? "localhost:8080" :  url1.getHost();
//                List<Application> appList = user.getApplications();
//
//                for (Application app : appList) {
//                        app.setUrl(app.getUrl().replace(app.getUrl().split("//")[1].split("/")[0], host1));
//                }

//            if (response.getCode() == ResultCode.ERROR && (response.getMessage() != null) && response.getMessage().getAz().equals("INVPARAMS")) {
//                operationResponse.setMessage(new MultilanguageString("İstifadəçi adı və ya şifrə yanlışdır!", 
//                                                                     "Invalid username/password!",  
//                                                                     "Hеправильное имя пользователя или пароль!"));
//                throw new Exception("Invalid username/password. Username: " + userProperties.getUsername());
//            }  else if (response.getCode() == ResultCode.ERROR) {
//                operationResponse.setMessage(new MultilanguageString("Əlaqədə problem var!", 
//                                                                     "Problem with connection!",  
//                                                                     "Проблема с подключением!"));
//                throw new Exception("Problem with connection");
//            }

//                if(user.getIsBlocked()) {
//                    operationResponse.setMessage(new MultilanguageString("İstifadəçi blok olunub!", 
//                                                                         "User blocked!",  
//                                                                         "Пользователь заблокирован!"));
//
//                    throw new Exception("User blocked" + userProperties.getUsername());
//                }

//            userProperties.setId(Long.valueOf(user.getId()));
//            userProperties.setRole(user.getEmail());
//            String token = jwtGenerator.generate(userProperties);
//            OperationResponse newApplicationSessionResponse = service.addUserSession(Long.valueOf(user.getId()), token.split("\\.")[2]);
//
//            if (newApplicationSessionResponse.getCode() != ResultCode.OK) {
//                operationResponse.setMessage(new MultilanguageString("Sessiya yaranmadi!", 
//                                                                     "Session is not create!",  
//                                                                     "Сессия не создает!"));
//                throw new Exception("Session is not create" + userProperties.getUsername());
//            }
//
//            new AuthController().addCacheOperation(token, user);
//
//            httpServletResponse.addHeader("Auth", token);
//
//            operationResponse.setCode(ResultCode.OK);
//            operationResponse.setData(user);
            return operationResponse;
            
 
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    
    @PostMapping(value = "/register")
    protected OperationResponse register(@RequestBody RegisterForm form,
                                           HttpServletRequest request,
                                           HttpServletResponse httpServletResponse,
                                           BindingResult result) {

        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            
            URL url1 = new URL(request.getRequestURL().toString());
            RegisterValidator validator = new RegisterValidator();
            validator.validate(form, result);
            if (result.hasErrors()) {
                operationResponse.setData(result.getAllErrors());
                operationResponse.setCode(ResultCode.INVALID_PARAMS);
                operationResponse.setMessage(new MultilanguageString("Parameterlərdə səhvlik var", 
                                                                     "Invalid parameters", 
                                                                     "Invalid parameters"));
                throw new Exception("Invalid params!!!");
            }
            String dbName = "flip";
            boolean userDetail = service.checkUserDetailsByEmail(form.getEmail());
            if(userDetail) {
                operationResponse.setMessage(new MultilanguageString("Email artiq movcuddur", "Email artiq movcuddur", "Email artiq movcuddur"));
                operationResponse.setCode(ResultCode.DUPLICATE_DATA);
                return operationResponse;
            }

            operationResponse = service.registerUser(form);

            if(!operationResponse.getCode().equals(ResultCode.OK)) {
                operationResponse.setMessage(new MultilanguageString("Əlaqədə problem var!", 
                                                                         "Problem with connection!",  
                                                                         "Проблема с подключением!"));
                throw new Exception("Problem with connection");
            }
            UserProperties userProperties = new UserProperties();
            userProperties.setUsername(form.getEmail());
            userProperties.setPassword(form.getPassword());
            
            OperationResponse response = service.doLogin(userProperties);

            User user = (User) response.getData();
            
            userProperties = new UserProperties();
            userProperties.setId(Long.valueOf(user.getId()));
            userProperties.setRole(user.getEmail());
            
            String token = jwtGenerator.generate(userProperties);
//            String token = Crypto.getDoubleGuid();
                    
            OperationResponse newApplicationSessionResponse = service.addUserSession(Long.valueOf(user.getId()), token);
            
            if (newApplicationSessionResponse.getCode() != ResultCode.OK) {
                operationResponse.setMessage(new MultilanguageString("Sessiya yaranmadi!", 
                                                                     "Session is not create!",  
                                                                     "Сессия не создает!"));
                throw new Exception("Session is not create" + userProperties.getUsername());
            }
            
            new AuthController().addCacheOperation(token, user);
            
            httpServletResponse.addHeader("Auth", token);
            
            operationResponse.setCode(ResultCode.OK);
            operationResponse.setData(user);

            return operationResponse;
            
 
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/confirmEmail")
    protected OperationResponse confirmEmail(@RequestBody UserForm form,
                                                HttpServletRequest request,
                                                HttpServletResponse httpServletResponse) {

        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            
            URL url1 = new URL(request.getRequestURL().toString());
            boolean userDetail = service.checkEmailConfirmCode(form.getEmail(), form.getConfirmCode());
            
            if(!userDetail) {
                operationResponse.setMessage(new MultilanguageString("Kod yanlışdır", "Kod yanlışdır", "Kod yanlışdır"));
                operationResponse.setCode(ResultCode.INVALID_PARAMS);
                return operationResponse;
            }
            
            UserProperties userProperties = new UserProperties();
            userProperties.setUsername(form.getEmail());
            userProperties.setPassword(form.getPassword());
//            OperationResponse response = service.doLoginForDefaultUser(userProperties, dbName, "0");

//            User user = (User) response.getData();
//
//            if (response.getCode() == ResultCode.ERROR && (response.getMessage() != null) && response.getMessage().getAz().equals("INVPARAMS")) {
//                operationResponse.setMessage(new MultilanguageString("İstifadəçi adı və ya şifrə yanlışdır!", 
//                                                                     "Invalid username/password!",  
//                                                                     "Hеправильное имя пользователя или пароль!"));
//                throw new Exception("Invalid username/password. Username: " + userProperties.getUsername());
//            }  else if (response.getCode() == ResultCode.ERROR) {
//                operationResponse.setMessage(new MultilanguageString("Əlaqədə problem var!", 
//                                                                     "Problem with connection!",  
//                                                                     "Проблема с подключением!"));
//                throw new Exception("Problem with connection");
//            }
//
//            userProperties.setId(Long.valueOf(user.getId()));
//            userProperties.setRole(user.getEmail());
//            String token = jwtGenerator.generate(userProperties);
//            OperationResponse newApplicationSessionResponse = service.addUserSession(Long.valueOf(user.getId()), token.split("\\.")[2]);
//
//            if (newApplicationSessionResponse.getCode() != ResultCode.OK) {
//                operationResponse.setMessage(new MultilanguageString("Sessiya yaranmadi!", 
//                                                                     "Session is not create!",  
//                                                                     "Сессия не создает!"));
//                throw new Exception("Session is not create" + userProperties.getUsername());
//            }
//
//            new AuthController().addCacheOperation(token, user);
//
//            httpServletResponse.addHeader("Auth", token);
//
//            operationResponse.setCode(ResultCode.OK);
//            operationResponse.setData(user);
//            return operationResponse;
            
 
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    
//    @PostMapping(value = "/forgetPassword")
//    protected OperationResponse forgetPassword(@RequestBody UserForm form,
//                                                HttpServletRequest request,
//                                                HttpServletResponse httpServletResponse) {
//
//        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
//        try {
//            
//            URL url1 = new URL(request.getRequestURL().toString());
//            boolean userDetail = service.checkUserDetailsByEmail(form.getEmail());
//            if(!userDetail) {
//                operationResponse.setMessage(new MultilanguageString("Email movcud deyil", "Email movcud deyil", "Email movcud deyil"));
//                operationResponse.setCode(ResultCode.INVALID_PARAMS);
//                return operationResponse;
//            }
//            String code = Crypto.randomNumber(6);
//            EmailUtils utils = new EmailUtils();
//            utils.sendUserMail(form.getEmail(), code, EmailMessage.FORGET_SUBJECT,
//                                                      EmailMessage.FORGET_H1,
//                                                      EmailMessage.FORGET_P);
//            
//            service.updateConfirmCode(form.getEmail(), code);
//            
//            operationResponse.setCode(ResultCode.CONFIRM_CODE);
//            return operationResponse;
//            
// 
//        } catch (Exception e) {
//            log.error(e.getMessage(), e);
//        }
//
//        return operationResponse;
//    }
    
    @PostMapping(value = "/checkConfirmCode")
    protected OperationResponse checkConfirmCode(@RequestBody UserForm form,
                                                HttpServletRequest request,
                                                HttpServletResponse httpServletResponse) {

        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            URL url1 = new URL(request.getRequestURL().toString());
            
            boolean b = service.checkConfirmCode(form.getEmail(), form.getConfirmCode());
            operationResponse.setCode(b ? ResultCode.OK : ResultCode.INVALID_PARAMS);
            return operationResponse;
 
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/updatePassword")
    protected OperationResponse updatePassword(@RequestBody UserForm form,
                                                HttpServletRequest request,
                                                HttpServletResponse httpServletResponse) {

        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            URL url1 = new URL(request.getRequestURL().toString());
            
            if(!form.getPassword().equals(form.getConfirmPassword())) {
                operationResponse.setMessage(new MultilanguageString("Təkrar şifrə düzgün deyil", 
                                                                     "Təkrar şifrə düzgün deyil",  
                                                                     "Təkrar şifrə düzgün deyil"));
                return operationResponse;
            }
            
            boolean b = service.updateNewPassword(form.getEmail(), form.getPassword(), form.getConfirmCode());
            operationResponse.setCode(b ? ResultCode.OK : ResultCode.ERROR);
            return operationResponse;
 
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
}
