/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.validator;

import com.flip.form.RegisterForm;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author Nazrin
 */
public class RegisterValidator implements Validator {
    
    
    @Override
    public boolean supports(Class<?> type) {
        return RegisterForm.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        RegisterForm form = (RegisterForm) o;
        
        if(form.getEmail() == null || form.getEmail().trim().isEmpty()) {
            errors.reject("errors.email.empty");
        }
        
        if(form.getPhone() == null || form.getPhone().trim().isEmpty()) {
            errors.reject("errors.email.empty");
        }
        
        if(form.getFullname() == null || form.getFullname().trim().isEmpty()) {
            errors.reject("errors.fullname.empty");
        }
        
        if(form.getPassword()== null || form.getPassword().trim().isEmpty()) {
            errors.reject("errors.password.empty");
        }
        
        if(!(form.getPassword()== null || form.getPassword().trim().isEmpty()) &&
             form.getPassword().length() < 8 ) {
            errors.reject("errors.password.lowerSize");
        }
        
        if(form.getConfirmPassword() == null || form.getConfirmPassword().trim().isEmpty()) {
            errors.reject("errors.confirmPassword.empty");
        }
        
        if(!(form.getPassword() == null || form.getPassword().trim().isEmpty()) &&
           !(form.getConfirmPassword() == null || form.getConfirmPassword().trim().isEmpty())  &&
             !form.getConfirmPassword().equals(form.getPassword())) {
            errors.reject("errors.confirmPassword.incorrect");
        }
        
    }
    
}
