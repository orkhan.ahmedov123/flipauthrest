/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.validator;

import com.flip.domain.UserProperties;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author Nazrin
 */
public class LoginValidator implements Validator {
    private final boolean isRestValidation;
    
    public LoginValidator(boolean isRestValidation) {
        this.isRestValidation = isRestValidation;
    }
    
    @Override
    public boolean supports(Class<?> type) {
        return UserProperties.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserProperties loginForm = (UserProperties) o;
        
        if(loginForm.getUsername() == null || loginForm.getUsername().trim().isEmpty()) {
            errors.reject("errors.login.username.empty");
        }
        if(loginForm.getPassword() == null || loginForm.getPassword().trim().isEmpty() ) {
            errors.reject("errors.login.password.empty");
        }
    }
    
}
