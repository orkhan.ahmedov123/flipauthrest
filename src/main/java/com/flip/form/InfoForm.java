/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.form;

import lombok.Data;

/**
 *
 * @author ahmadov
 */
@Data
public class InfoForm {
    private String email = "";
    private String phone = "";
    private String oldPassword = "";
    private String newPassword = "";
    private String confirmNewPassword = "";
    private String fullname = "";
    private String photoFileId = "0";
}
