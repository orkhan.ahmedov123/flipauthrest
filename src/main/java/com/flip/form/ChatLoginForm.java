/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.form;

import lombok.Data;

/**
 *
 * @author ahmadov
 */
@Data
public class ChatLoginForm {
    private String accessKey = "";
    private String accessId = "";
    private String orgId = "0";
    private String orgName = "";
    private String positionName = "";
    private String contact = "";
    private String username = "";
    private String firstname = "";
    private String lastname = "";
    private String userType = "USER";
    private String orgParentId = "";
    private String orgParentName = "";
}
