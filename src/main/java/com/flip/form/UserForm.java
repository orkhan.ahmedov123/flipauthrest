/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.form;

import lombok.Data;

/**
 *
 * @author ahmadov
 */
@Data
public class UserForm {
    private String id = "";
    private String username = "";
    private String password = "";
    private String confirmPassword = "";
    private String oldPassword = "";
    private String name = "";
    private String surname = "";
    private String patronymic = "";
    private String birthdate = "";
    private String photoFileId = "";
    private String genderId = "";
    private String email = "";
    private String token = "";
    private String fileName = "";
    private String filePath = "";
    private String confirmCode = "";
}
