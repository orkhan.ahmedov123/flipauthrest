/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.form;

import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class RegisterForm {
    private String email;
    private String phone;
    private String password;
    private String confirmPassword;
    private String fullname;
    
}
