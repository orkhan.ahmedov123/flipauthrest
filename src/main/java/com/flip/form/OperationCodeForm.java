/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.form;

import lombok.Data;

/**
 *
 * @author Asan
 */
@Data
public class OperationCodeForm {
    private String operationCode;
    private String announcementId;
    private String announcementType;
}
