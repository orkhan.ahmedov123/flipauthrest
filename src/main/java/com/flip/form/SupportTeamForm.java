/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.form;

import lombok.Data;

/**
 *
 * @author Asan
 */
@Data
public class SupportTeamForm {
    private String id = "";
    private String operationType = "";
    private String orgId = "";
    private String supportUsers = "";
    private String orgName = "";
    private String chatMessage = "";
    private String unreadMessageUser = "";
    private String replyUserId = "";
    private String message = "";
}
