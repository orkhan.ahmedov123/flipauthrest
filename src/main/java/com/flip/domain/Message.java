/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import lombok.Data;

/**
 *
 * @author Asan
 */
@Data
public class Message {
    private String message;
    private String date;
    private String createUserId;
    private String firstname;
    private String lastname;
    private String chatUserType;

    public Message(String message, String date, String createUserId ) {
        this.message = message;
        this.date = date;
        this.createUserId = createUserId;
    }

    public Message(String message, String date, String createUserId, String firstname, String lastname, String chatUserType) {
        this.message = message;
        this.date = date;
        this.createUserId = createUserId;
        this.firstname = firstname;
        this.lastname = lastname;
        this.chatUserType = chatUserType;
    }
    
    
}
