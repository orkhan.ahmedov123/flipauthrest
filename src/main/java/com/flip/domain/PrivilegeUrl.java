/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class PrivilegeUrl {
    private String url;
    private String type;

    public PrivilegeUrl(String url, String type) {
        this.url = url;
        this.type = type;
    }
    
    
}
