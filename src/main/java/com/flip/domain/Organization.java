/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class Organization {
    private String id;
    private String orgTypeId;
    private MultilanguageString name;
    private MultilanguageString shortName;
    private String formula;
    private String logoName;
    private String serverName;
    private String description;
    private FileWrapper logo;

    public Organization(String id, String orgTypeId, MultilanguageString name, MultilanguageString shortName, String formula) {
        this.id = id;
        this.orgTypeId = orgTypeId;
        this.name = name;
        this.shortName = shortName;
        this.formula = formula;
    }

    public Organization(String id, String orgTypeId, MultilanguageString name, MultilanguageString shortName, String formula, FileWrapper logo) {
        this.id = id;
        this.orgTypeId = orgTypeId;
        this.name = name;
        this.shortName = shortName;
        this.formula = formula;
        this.logo = logo;
    }
    
    public Organization(String id, String orgTypeId, MultilanguageString name, MultilanguageString shortName, String formula, String logoName, String serverName, String description) {
        this.id = id;
        this.orgTypeId = orgTypeId;
        this.name = name;
        this.shortName = shortName;
        this.formula = formula;
        this.logoName = logoName;
        this.serverName = serverName;
        this.description = description;
    }
}
