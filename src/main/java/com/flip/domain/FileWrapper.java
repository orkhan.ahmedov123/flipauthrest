/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class FileWrapper {
    private String id;
    private String path;
    private String originalName;
    private String contentType;
    private String fileSize;
    private String createUser;

    public FileWrapper(String id, String path, String originalName) {
        this.id = id;
        this.path = path;
        this.originalName = originalName;
    }

    public FileWrapper(String id) {
        this.id = id;
    }

    public FileWrapper(String id, String path, String originalName, String contentType) {
        this.id = id;
        this.path = path;
        this.originalName = originalName;
        this.contentType = contentType;
    }

    public FileWrapper(String id, String path, String originalName, String contentType, String createUser) {
        this.id = id;
        this.path = path;
        this.originalName = originalName;
        this.contentType = contentType;
        this.createUser = createUser;
    }

    public FileWrapper(String id, String path, String originalName, String contentType, String fileSize, String createUser) {
        this.id = id;
        this.path = path;
        this.originalName = originalName;
        this.contentType = contentType;
        this.fileSize = fileSize;
        this.createUser = createUser;
    }
    
    
}
