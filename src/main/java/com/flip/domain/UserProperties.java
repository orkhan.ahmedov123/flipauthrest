/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import java.util.List;

/**
 *
 * @author otahmadov
 */
public class UserProperties {
    
    private long id;
    private String username;
    private String password;
    private String role;
    private String ipAddress;
//    private List<MOdul> role;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "JwtUser{" + "id=" + id + ", username=" + username + ", password=" + password + ", role=" + role + ", ipAddress=" + ipAddress + '}';
    }
    
}
