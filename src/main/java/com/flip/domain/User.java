/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import java.util.Date;
import java.util.List;
import lombok.Data;

/**
 *
 * @author ahmadov
 */
@Data
public class User {
    private String id;
    private String email;
    private String phone;
    private String userType;
    private String registerType;
    private String photoFileId;
    private String fullname;
    private String createDate;
    private String flipCoin;
    private List<String> privilegeUrlList;

    public User() {
        
    }

    public User(String id, String flipCoin, String email, String phone, String userType, String registerType, String photoFileId, String fullname, String createDate, List<String> privilegeUrlList) {
        this.id = id;
        this.flipCoin = flipCoin;
        this.email = email;
        this.phone = phone;
        this.userType = userType;
        this.registerType = registerType;
        this.photoFileId = photoFileId;
        this.fullname = fullname;
        this.createDate = createDate;
        this.privilegeUrlList = privilegeUrlList;
    }
    
    
}
