/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import lombok.Data;

/**
 *
 * @author ahmadov
 */
@Data
public class AccountUsers {
    private String id;
    private String accountId;
    private Organization organization;
    private String userType;
    private DictionaryWrapper role;
    private String currentStatus;

    public AccountUsers(String id, String accountId, Organization organization, String userType, DictionaryWrapper role, String currentStatus) {
        this.id = id;
        this.accountId = accountId;
        this.organization = organization;
        this.userType = userType;
        this.role = role;
        this.currentStatus = currentStatus;
    }
    
    
}
