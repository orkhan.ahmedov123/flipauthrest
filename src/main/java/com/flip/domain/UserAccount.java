/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import java.util.List;
import lombok.Data;

/**
 *
 * @author ahmadov
 */
@Data
public class UserAccount {
    private String id;
    private String username;
    private String password;
    private DictionaryWrapper role;

    public UserAccount() {
    }

    public UserAccount(String id, DictionaryWrapper role) {
        this.id = id;
        this.role = role;
    }
}
