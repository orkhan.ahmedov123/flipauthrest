/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import java.util.List;
import lombok.Data;

/**
 *
 * @author ahmadov
 */
@Data
public class Application {
    private String id;
    private MultilanguageString name;
    private MultilanguageString shortName;
    private String parentId;
    private String url;
    private int orderBy;
    private String icon;
    private String code;
    private List<Module> modules;
    private String newType;

    public Application(String id, MultilanguageString name, MultilanguageString shortName, String parentId, String url, int orderBy, String icon, List<Module> modules) {
        this.id = id;
        this.name = name;
        this.shortName = shortName;
        this.parentId = parentId;
        this.orderBy = orderBy;
        this.url = url;
        this.icon = icon;
        this.modules = modules;
    }

    public Application(String id, MultilanguageString name, MultilanguageString shortName, String parentId, String url, int orderBy, String icon, String newType, List<Module> modules) {
        this.id = id;
        this.name = name;
        this.shortName = shortName;
        this.parentId = parentId;
        this.orderBy = orderBy;
        this.url = url;
        this.icon = icon;
        this.newType = newType;
        this.modules = modules;
    }
}
