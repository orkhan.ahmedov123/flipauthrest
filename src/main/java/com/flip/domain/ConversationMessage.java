/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import lombok.Data;

/**
 *
 * @author ahmadov
 */
@Data
public class ConversationMessage {
    private String id;
    private String conversationId;
    private String fromUserId;
    private String fromUserFullname;
    private String fromUserPhotoFileId;
    private String createTime;
    private String messsageText;
    private String messsageFileId;
    private String readStatus;
    private String status;
    private String chatUserType;
}
