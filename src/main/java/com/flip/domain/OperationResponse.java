/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import com.flip.enums.ResultCode;
import lombok.Data;

/**
 *
 * @author Orkhan
 */
@Data
public class OperationResponse {
    private MultilanguageString message;
    private Object data;
    private ResultCode code;

    public OperationResponse() {
    }

    public OperationResponse(MultilanguageString message, Object data, ResultCode code) {
        this.message = message;
        this.data = data;
        this.code = code;
    }

    public OperationResponse(Object data, ResultCode code) {
        this.data = data;
        this.code = code;
    }

    public OperationResponse(MultilanguageString message, ResultCode code) {
        this.message = message;
        this.code = code;
    }

    public OperationResponse(ResultCode code) {
        this.code = code;
    }
}
