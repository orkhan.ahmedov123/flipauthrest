/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import lombok.Data;

/**
 *
 * @author ahmadov
 */
@Data 
public class ConversationUser {
    private String id;
    private String userId;

    public ConversationUser() {
    }

    
    public ConversationUser(String id, String userId) {
        this.id = id;
        this.userId = userId;
    }
    
    
}
