/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import lombok.Data;

/**
 *
 * @author Orkhan
 */
@Data
public class DictionaryWrapper {
    private String id;
    private DictionaryWrapper parentId;
    private MultilanguageString value;
    private MultilanguageString aboutValue;
    private String icon;
    private String code;
    private MultilanguageString type;
    private String unreadMessageCount;

    public DictionaryWrapper(String id, MultilanguageString value) {
        this.id = id;
        this.value = value;
    }

    public DictionaryWrapper(String id, MultilanguageString value, String unreadMessageCount) {
        this.id = id;
        this.value = value;
        this.unreadMessageCount = unreadMessageCount;
    }

    public DictionaryWrapper() {
    }

    public DictionaryWrapper(String id) {
        this.id = id;
    }

    public DictionaryWrapper(MultilanguageString value) {
        this.value = value;
    }

    public DictionaryWrapper(String id, DictionaryWrapper parentId, MultilanguageString value, MultilanguageString aboutValue, String icon, String code, MultilanguageString type) {
        this.id = id;
        this.parentId = parentId;
        this.value = value;
        this.aboutValue = aboutValue;
        this.icon = icon;
        this.code = code;
        this.type = type;
    }
    
}
