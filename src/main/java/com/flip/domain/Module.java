/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import java.util.List;
import lombok.Data;

/**
 *
 * @author ahmadov
 */
@Data 
public class Module {

    private String id;
    private MultilanguageString name;
    private MultilanguageString shortName;
    private String parentId;
    private int orderBy;
    private String icon;
    private String code;
    private String newType;
    private List<Operations> operations;
    
    
    public Module() {
    }

    public Module(String id, MultilanguageString name, MultilanguageString shortName, String icon, String code, List<Operations> operations) {
        this.id = id;
        this.name = name;
        this.shortName = shortName;
        this.icon = icon;
        this.code = code;
        this.operations = operations;
    }

    public Module(String id, MultilanguageString name, MultilanguageString shortName, String icon, String code, String newType, List<Operations> operations) {
        this.id = id;
        this.name = name;
        this.shortName = shortName;
        this.icon = icon;
        this.code = code;
        this.newType = newType;
        this.operations = operations;
    }

}
