/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import lombok.Data;

/**
 *
 * @author Lito
 */
@Data
public class Operations {
    
    private String id;
    private String moduleId;
    private String url;
    private String code;
    private MultilanguageString name;
    private String newType;

    public Operations(String id, String moduleId, String url, MultilanguageString name) {
        this.id = id;
        this.moduleId = moduleId;
        this.url = url;
        this.name = name;
    }

    public Operations(String id, String url) {
        this.id = id;
        this.url = url;
    }

    public Operations(String id, String moduleId, String url, String code, MultilanguageString name) {
        this.id = id;
        this.moduleId = moduleId;
        this.url = url;
        this.code = code;
        this.name = name;
    }
    
    public Operations(String id, String moduleId, String url, String code, MultilanguageString name, String newType) {
        this.id = id;
        this.moduleId = moduleId;
        this.url = url;
        this.code = code;
        this.name = name;
        this.newType = newType;
    }
    
    
}
