/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.filter;

import com.flip.enums.SecurityConstants;
import com.flip.security.JwtAuthenticationToken;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

/**
 *
 * @author ahmadov
 */

public class JwtAuthenticationTokenFilter extends AbstractAuthenticationProcessingFilter{
    
    public JwtAuthenticationTokenFilter() {
        super("/api/jwt/**");
    }
    
    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws AuthenticationException, IOException, ServletException {
       String header = httpServletRequest.getHeader("Auth");
       
       if((header == null || !header.startsWith("Education ")) && !HttpMethod.OPTIONS.name().equals(httpServletRequest.getMethod())) {
//           System.out.println("JWT token is missing" + header);
           throw new RuntimeException("JWT token is missing");
       }
       
       if(HttpMethod.OPTIONS.name().equals(httpServletRequest.getMethod())) {
           JwtAuthenticationToken optionToken = new JwtAuthenticationToken(SecurityConstants.OPTION_REQUEST);
           return getAuthenticationManager().authenticate(optionToken); 
       }
       
       
       
       String ssid = header.substring(10);
       JwtAuthenticationToken token = new JwtAuthenticationToken(ssid);
       return getAuthenticationManager().authenticate(token);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        super.successfulAuthentication(request, response, chain, authResult);
        chain.doFilter(request, response);
    }
    
    
  
}
