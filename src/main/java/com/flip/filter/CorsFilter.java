/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.filter;

import com.flip.domain.XSSRequestWrapper;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.filter.GenericFilterBean;

/**
 *
 * @author Orkhan
 */
public class CorsFilter extends GenericFilterBean {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
       HttpServletResponse httpServletResponse = (HttpServletResponse) response;
       HttpServletRequest httpServletRequest = (HttpServletRequest) request;
            httpServletResponse.setHeader("Content-Security-Policy", "default-src * 'unsafe-inline' 'unsafe-eval';frame-ancestors 'self'");
            httpServletResponse.setHeader("X-XSS-Protection", "1; mode=block");
            httpServletResponse.setHeader("X-Frame-Options", "SAMEORIGIN"); //butun ifaremelerde istifadeni mehdudlasdirir
            httpServletResponse.setHeader("X-Content-Type-Options", "nosniff");
            httpServletResponse.setHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
            httpServletResponse.setHeader("Pragma", "no-cache");
            httpServletResponse.setHeader("Expires", "0");
            httpServletResponse.setHeader("Strict-Transport-Security", "max-age=31536000 ; includeSubDomains");// https ucun istifade edilir    

//            httpServletResponse.setHeader("Access-Control-Allow-Origin", httpServletRequest.getHeader("Origin"));
            httpServletResponse.setHeader("Access-Control-Allow-Headers", "*");
            httpServletResponse.setHeader("Access-Control-Expose-Headers", "Auth");
           chain.doFilter(new XSSRequestWrapper((HttpServletRequest) request), response);
           
       
    }
    
}
