/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.enums;

/**
 *
 * @author Nazrin
 */
public enum ResultCode {
    OK,
    ERROR,
    UNAUTHORIZED,
    INVALID_PARAMS,
    DUPLICATE_DATA,
    INVALID_DATE,
    ONLINE,
    OFFLINE,
    NEW_MESSAGE,
    NEW_BOT_MESSAGE,
    REQUEST_USER,
    APPLY_REQUEST,
    REMOVE_REQUEST,
    DELETE_CONVERSATION,
    DELETE_MESSAGE,
    BLOCK_USER,
    READ_MESSAGE,
    CALLING,
    DECLINE,
    CONFIRM_CODE,
    NEW_ROOM
}
