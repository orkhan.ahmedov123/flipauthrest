/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.enums;

import com.flip.util.ExcellUtil;
import java.util.List;
import java.util.Map;

/**
 *
 * @author otahmadov
 */
public class SqlConstants {
    
    public static final String USER_QUERY = "select * from v_users WHERE id = ? ";
    
    public static final String DO_LOGIN_QUERY = "SELECT * FROM v_users u " +
                                                            "WHERE LOWER (email) = LOWER (?)  AND password = ?  AND active = 1";
    
    public static final String DO_LOGIN_QUERY_FOR_EMPRO = "SELECT id, lang_id, in_system " +
                                                            "FROM users u " +
                                                            "WHERE (LOWER (username) = LOWER (?) or LOWER (email) = LOWER (?))  AND password = ?  AND active = 1";
    
    public static final String DO_LOGIN_QUERY_FOR_EMPRO_WITHOUT_PASSWORD = "SELECT id, lang_id, in_system  " +
                                                            "FROM users u " +
                                                            "WHERE (LOWER (username) = LOWER (?) or LOWER (email) = LOWER (?))  AND active = 1";
    
    public static final String GET_USER_BY_ACCOUNT = "SELECT USER_ID, " +
                                            "org_id, " +
                                            "org_AZ, " +
                                            "org_en, " +
                                            "org_ru, " +
                                            "org_short_AZ, " +
                                            "org_short_en, " +
                                            "org_short_ru, " +
                                            "UNI_ID, " +
                                            "UNI_AZ, " +
                                            "UNI_EN, " +
                                            "UNI_RU, " +
                                            "UNI_SHORT_AZ, " +
                                            "UNI_SHORT_EN, " +
                                            "UNI_SHORT_RU, " +
                                            "USER_ACCOUNT_ID " +
                                            "FROM UB_SECURITY.V_ALL_USERS U " +
                                            "WHERE U.USER_ACCOUNT_ID = ?";
    
    public static final String GET_APPLICATION_QUERY = " SELECT * FROM V_USER_APPLICATION_list  " +
                                                   " WHERE USER_ID = ? " +
                                                   " ORDER BY ORDER_BY ";
    
    public static final String GET_MODULE_QUERY = "SELECT * from V_user_MODULE_list WHERE USER_ID = ? AND APPlication_ID = ? ";
    
    public static final String GET_OPERATION_QUERY = "select * from V_USER_OPERATION_list WHERE USER_ID = ?  AND MODULE_ID = ? ORDER BY url";
    
    public static final String GET_ALL_PRIVILEGE_QUERY = "SELECT URL FROM ALL_PRIVILEGE ";
    
    public static final String ADD_USER_SESSION = "insert into user_session (id, token, user_id, ip_address, device, active, create_date, create_user_id) "
                                                                            + " values(?,?,?,?,?,1, now(), ?)";
    
    
    public static String generateMigrationFile(String key, Map<String,List<Map<String,String>>> map) {
       int index = 0;
        String query = "do $$ declare begin ";
            for(Map<String,String> m: map.get(key)) {
                ++index;
                query += " insert into " + key + " (";
                for(String s: m.keySet()) {
                   query += s + ","; 
                }
                query = query.substring(0, query.length() - 1) + ") values (";
                for(String s: m.keySet()) {
                    String val = "'"+m.get(s)+"'";
                   query += val + ","; 
                }
                query = query.substring(0, query.length() - 1) + "); ";
            }
            
            query += " end $$;";
            
            return query;
    }
}
