/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author otahmadov
 */
public class SecurityConstants {
    public static final String SECRET = "FlipSecurityJWTs";
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/users/sign-up";
    public static final String OPTION_REQUEST = "ThisRequestIsOptionRequest";
    
    public static boolean checkPrivilegeUrl(String url) {
        
        if(allCommonUrl.contains(url)) return true;
        
        return false;
    }
    public static boolean checkUrlForChangeCoin(String url) {
        
        if(forChangeCoinUrl.contains(url.toLowerCase())) return true;
        
        return false;
    }
    
    public static List<String> allCommonUrl = Arrays.asList(
        "/common/FlipSystem/Sale/AddApartment",
        "/common/FlipSystem/Sale/EditApartment",
        "/common/FlipSystem/Sale/DeleteApartment",
        "/common/FlipSystem/Sale/GetApartmentList",
        "/common/FlipSystem/Sale/Add",
        "/common/FlipSystem/Sale/Edit",
        "/common/FlipSystem/Sale/Delete",
        "/common/FlipSystem/Sale/GetSaleList",
        "/common/FlipSystem/Sale/AdvanceSearch",
        "/common/FlipSystem/Rent/Add",
        "/common/FlipSystem/Rent/Edit",
        "/common/FlipSystem/Rent/Delete",
        "/common/FlipSystem/Rent/GetRentList",
        "/common/FlipSystem/Rent/AdvanceSearch",
        "/common/FlipSystem/Dictionaries/GetDictionariesListByCommon",
        "/common/FlipSystem/Dictionaries/GetDictionariesListByMultiCode",
        "/common/FlipSystem/Sale/GetSimilarAnnouncement",
        "/common/FlipSystem/Rent/GetSimilarAnnouncement",
        "/common/FlipSystem/Sale/ForgetOperationCode",
        "/common/FlipSystem/Rent/ForgetOperationCode"
        
    );
    
    public static List<String> forChangeCoinUrl = Arrays.asList(
        "rent/add",
        "sale/add",
        "rent/pushtheadforward",
        "sale/pushtheadforward"
    );
    
    
    
}
