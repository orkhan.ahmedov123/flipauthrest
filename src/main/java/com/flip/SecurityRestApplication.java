package com.flip;

import javax.servlet.MultipartConfigElement;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

@SpringBootApplication
@EnableAutoConfiguration
@EnableCaching
@Configuration
public class SecurityRestApplication {
    
	public static void main(String[] args) {
            
		SpringApplication.run(SecurityRestApplication.class, args);
	}
    @Bean(name = "multipartResolver")//configuration ile isletmek lazimdir
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(54525952); //...specify your size of file  (20971520 - 20 MB) (54525952 - 52 MB)
        return multipartResolver;
    }
}
