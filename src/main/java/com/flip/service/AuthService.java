/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.service;

import com.flip.dao.AuthDao;
import com.flip.domain.UserProperties;
import com.flip.domain.OperationResponse;
import com.flip.form.CheckForm;
import com.flip.form.InfoForm;
import com.flip.form.OperationCodeForm;
import com.flip.form.RegisterForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ahmadov
 */
@Service
public class AuthService implements IAuthService {

    @Autowired
    private AuthDao securityDao;
    
    @Override
    public OperationResponse checkToken(String token) {
        return securityDao.checkToken(token);
    }
    
    @Override
    public OperationResponse doLogin(UserProperties jwtUser) {
        return securityDao.doLogin(jwtUser);
    }
    
    @Override
    public OperationResponse doExit(String token) {
        return securityDao.doExit(token);
    }
    
    
    @Override
    public OperationResponse addUserSession(long userId, String token) {
        return securityDao.addUserSession(userId, token);
    }

    @Override
    public OperationResponse uploadFileAndGetId(String originalName, String contentType, String path, long fileSize, String userId) {
        return securityDao.uploadFileAndGetId(originalName, contentType, path, fileSize, userId);
    }


    @Override
    public OperationResponse removeFile(String id, String userId) {
        return securityDao.removeFile(id, userId);
    }

    @Override
    public OperationResponse getFileById(String id) {
        return securityDao.getFileById(id);
    }

    @Override
    public OperationResponse getFileByIdAndCheckUser(String id, String userId) {
        return securityDao.getFileByIdAndCheckUser(id, userId);
    }

    
    @Override
    public OperationResponse changeUserInfo(InfoForm form, String id, String type)  {
        return securityDao.changeUserInfo(form, id, type);
    }
    
    @Override
    public boolean checkUserDetailsByEmail(String email)  {
        return securityDao.checkUserDetailsByEmail(email);
    }
    
    @Override
    public boolean checkEmailConfirmCode(String email, String code)  {
        return securityDao.checkEmailConfirmCode(email, code);
    }
    
    
    @Override
    public OperationResponse registerUser(RegisterForm form) {
        return securityDao.registerUser(form);
    }
    
    @Override
    public boolean updateConfirmCode(String email, String confirmCode) {
         return securityDao.updateConfirmCode(email, confirmCode);
    }
    
    @Override
    public boolean checkConfirmCode(String email, String confirmCode) {
         return securityDao.checkConfirmCode(email, confirmCode);
    }
    
    @Override
    public boolean updateNewPassword(String email, String password, String code) {
         return securityDao.updateNewPassword(email, password, code);
    }
    
    @Override
    public OperationResponse checkEmailOrPhone(CheckForm form) {
         return securityDao.checkEmailOrPhone(form);
    }
    
    @Override
    public OperationResponse checkAnnouncementOperationCode(OperationCodeForm form) {
         return securityDao.checkAnnouncementOperationCode(form);
    }
    
    @Override
    public String getUserCoin(String userId) {
         return securityDao.getUserCoin(userId);
    }
    
}
