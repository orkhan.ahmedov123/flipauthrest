/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.service;

import com.flip.domain.DictionaryWrapper;
import com.flip.domain.UserProperties;
import com.flip.domain.OperationResponse;
import com.flip.domain.Operations;
import com.flip.domain.User;
import com.flip.form.ChatLoginForm;
import com.flip.form.CheckForm;
import com.flip.form.InfoForm;
import com.flip.form.OperationCodeForm;
import com.flip.form.RegisterForm;
import com.flip.form.SupportTeamForm;
import com.flip.form.UserForm;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ahmadov
 */
public interface IAuthService {
    public OperationResponse checkToken(String token);
    public OperationResponse doLogin(UserProperties jwtUser);
    public OperationResponse doExit(String token);
    public OperationResponse addUserSession(long userId, String token);
    public OperationResponse uploadFileAndGetId(String originalName, String contentType, String path, long fileSize, String userId);
    public OperationResponse removeFile(String id, String userId);
    public OperationResponse getFileById(String id);
    public OperationResponse getFileByIdAndCheckUser(String id, String userId);
    public OperationResponse changeUserInfo(InfoForm form, String id, String type);
    public boolean checkUserDetailsByEmail(String email);
    public boolean checkEmailConfirmCode(String email, String code);
    public OperationResponse registerUser(RegisterForm form);
    public boolean updateConfirmCode(String email, String confirmCode);
    public boolean checkConfirmCode(String email, String confirmCode);
    public boolean updateNewPassword(String email, String password, String code);
    
    public OperationResponse checkEmailOrPhone(CheckForm form);
    public OperationResponse checkAnnouncementOperationCode(OperationCodeForm form);
    public String getUserCoin(String userId);
}
