/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.service;

import com.flip.domain.MultilanguageString;
import com.flip.domain.OperationResponse;
import com.flip.enums.ResultCode;
import com.flip.util.FtpUtils;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;
import org.apache.commons.io.IOUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;
import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author ahmadov
 */
@Service
public class FtpService {
    private static final Logger log = Logger.getLogger(FtpService.class);
    
    @Value("${ftp.host}")
    private String host;
    
    @Value("${ftp.port}")
    private int port;
    
    @Value("${ftp.username}")
    private String username;
    
    @Value("${ftp.password}")
    private String password;
    
    
    public OperationResponse saveFtpFile(String directory, MultipartFile file, String name) throws IOException {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        FTPClient client = new FTPClient();
        
        try {
            
            if(FtpUtils.getExtension(file.getContentType()) == null || FtpUtils.getExtension(file.getContentType()).trim().isEmpty()) {
                operationResponse.setCode(ResultCode.INVALID_PARAMS);
                operationResponse.setMessage(new MultilanguageString("Faylın tipi yanlışdır!!!", "Invalid file Type!!!", "Invalid file Type!!!"));
                return operationResponse;
            }
            client.connect(host, port);
            
            if(!client.login(username, password)) {
                throw new Exception("Invalid ftp username/password");
            }
            
            client.enterLocalPassiveMode();
            client.changeWorkingDirectory(directory);
            
            try(InputStream inputStream = file.getInputStream()) {
                client.setFileType(FTP.BINARY_FILE_TYPE);
                String fullPath = directory + "/" + name + FtpUtils.getExtension(file.getContentType());
                String[] pathArray = fullPath.split("/");
                StringBuilder sb = new StringBuilder();
                for(int i = 1; i < pathArray.length - 1; i++) {
                    String dir = pathArray[i];
                    sb.append("/" + dir);
                    if(!client.changeWorkingDirectory(sb.toString())) {
                        client.makeDirectory(sb.toString());
                    }
                    
                }
                System.out.println(fullPath);
                if(client.storeFile(fullPath, inputStream))  {
                    operationResponse.setData(fullPath);
                    operationResponse.setCode(ResultCode.OK);
                }
            }
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        finally {
            if(client.isConnected()) {
                client.logout();
                client.disconnect();
            }
        }
        
        return operationResponse;
    }
    public OperationResponse saveFtpFile2(String directory, InputStream inputStream, String name) throws IOException {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        FTPClient client = new FTPClient();
        
        try {
            client.connect(host, port);
            
            if(!client.login(username, password)) {
                throw new Exception("Invalid ftp username/password");
            }
            
            client.enterLocalPassiveMode();
            client.changeWorkingDirectory(directory);
            
//            try(InputStream inputStream = file.getInputStream()) {
                client.setFileType(FTP.BINARY_FILE_TYPE);
//                String fullPath = directory + "/" + name + FtpUtils.getExtension(file.getContentType());
                String fullPath = directory + "/" + name + ".png";
                String[] pathArray = fullPath.split("/");
                StringBuilder sb = new StringBuilder();
                for(int i = 1; i < pathArray.length - 1; i++) {
                    String dir = pathArray[i];
                    sb.append("/" + dir);
                    if(!client.changeWorkingDirectory(sb.toString())) {
                        client.makeDirectory(sb.toString());
                    }
                    
                }
                if(client.storeFile(fullPath, inputStream))  {
                    operationResponse.setData(fullPath);
                    operationResponse.setCode(ResultCode.OK);
                }
//            }
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        finally {
            if(client.isConnected()) {
                client.logout();
                client.disconnect();
            }
        }
        
        return operationResponse;
    }
    
    public OperationResponse saveSFtpFile(String directory, MultipartFile file, String name) throws IOException {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        JSch jsch = new JSch();
        Session session = null;
        
        try {
            if(FtpUtils.getExtension(file.getContentType()) == null || FtpUtils.getExtension(file.getContentType()).trim().isEmpty()) {
                operationResponse.setCode(ResultCode.INVALID_PARAMS);
                operationResponse.setMessage(new MultilanguageString("Faylın tipi yanlışdır!!!", "Invalid file Type!!!", "Invalid file Type!!!"));
                return operationResponse;
            }
            String fullPath = directory + name + FtpUtils.getExtension(file.getContentType());
            
            session = jsch.getSession(username, host, 22);
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword(password);
            session.connect();
            Channel channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp sftpChannel = (ChannelSftp) channel;
            String[] folders = directory.split( "/" );
            int count = 0;
            for ( String folder : folders ) {
                if ( count > 2 ) {
                    try {
                        sftpChannel.cd( folder );
                    }
                    catch ( Exception e ) {
                        sftpChannel.mkdir( folder );
                        sftpChannel.cd( folder );
                    }
                }
                ++count;
            }
            sftpChannel.put(file.getInputStream(), fullPath);
//            InputStream in = sftpChannel.get( "remote-file" );
            sftpChannel.exit();
            session.disconnect();
            operationResponse.setData(fullPath);
            operationResponse.setCode(ResultCode.OK);
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    public OperationResponse saveFtpFile(MultipartFile file, String name) throws IOException {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        FTPClient client = new FTPClient();
        
        try {
            client.connect(host, port);
            
            if(!client.login(username, password)) {
                throw new Exception("Invalid ftp username/password");
            }
            
            client.enterLocalPassiveMode();
            client.changeWorkingDirectory("");
            
            try(InputStream inputStream = file.getInputStream()) {
                client.setFileType(FTP.BINARY_FILE_TYPE);
                String fullPath = name + FtpUtils.getExtension(file.getContentType());
                
                if(client.storeFile(fullPath, inputStream))  {
                    operationResponse.setData(fullPath);
                    operationResponse.setCode(ResultCode.OK);
                }
            }
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        finally {
            if(client.isConnected()) {
                client.logout();
                client.disconnect();
            }
        }
        
        return operationResponse;
    }
    
     public OperationResponse saveFtpFileResize(String directory, InputStream file, String name, String contentType) throws IOException {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        FTPClient client = new FTPClient();
        
        try {
            
            client.connect(host, port);
            
            if(!client.login(username, password)) {
                throw new Exception("Invalid ftp username/password");
            }
            
            client.enterLocalPassiveMode();
            client.changeWorkingDirectory(directory);
            
            try(InputStream inputStream = file) {
                client.setFileType(FTP.BINARY_FILE_TYPE);
                String fullPath = directory + "/" + name + FtpUtils.getExtension(contentType);
                
                if(client.storeFile(fullPath, inputStream))  {
                    operationResponse.setData(fullPath);
                    operationResponse.setCode(ResultCode.OK);
                }
            }
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        finally {
            if(client.isConnected()) {
                client.logout();
                client.disconnect();
            }
        }
        
        return operationResponse;
    }
    
    public byte[] downloadFtpFile(String filePath) throws IOException {
        
        FTPClient client = new FTPClient();
        
        try {
            client.connect(host, port);
            if(!client.login(username, password)) {
                throw new Exception("Invalid ftp username/password");
            }
            
            client.enterLocalPassiveMode();
            
            try(ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
                client.setFileType(FTP.BINARY_FILE_TYPE);
                client.retrieveFile(filePath, outputStream);
                return outputStream.toByteArray();
            }
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        finally {
            if(client.isConnected()) {
                client.logout();
                client.disconnect();
            }
        }
        
        return null;
    }
    
    public byte[] downloadFtpFileWithSize(String filePath, String size) throws IOException {
        
        FTPClient client = new FTPClient();
        
        try {
            client.connect(host, port);
            if(!client.login(username, password)) {
                throw new Exception("Invalid ftp username/password");
            }
            
            client.enterLocalPassiveMode();
            client.setFileType(FTP.BINARY_FILE_TYPE);
            InputStream is = client.retrieveFileStream(filePath);
            BufferedImage img = ImageIO.read(is);
            int startSize = Integer.parseInt(size.split("x")[0]);
            int endSize = Integer.parseInt(size.split("x")[1]);
            BufferedImage scaledImg = Scalr.resize(img, startSize, endSize);
//                
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            ImageIO.write(scaledImg, "png", baos);

            return baos.toByteArray();
            
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        finally {
            if(client.isConnected()) {
                client.logout();
                client.disconnect();
            }
        }
        
        return null;
    }
//    
//    public InputStream getFileInputStream(String filePath) throws IOException {
//        
//        FTPClient client = new FTPClient();
//        
//        try {
//            client.connect(host, port);
//            if(!client.login(username, password)) {
//                throw new Exception("Invalid ftp username/password");
//            }
//            
//            InputStream inputStream = client.retrieveFileStream(filePath);
//            return inputStream;
//        }
//        catch(Exception e) {
//            log.error(e.getMessage(), e);
//        }
//        finally {
//            if(client.isConnected()) {
//                client.logout();
//                client.disconnect();
//            }
//        }
//        
//        return null;
//    }
    
    public byte[] downloadSFtpFile(String filePath) throws IOException {
        
       JSch jsch = new JSch();
        Session session = null;
        
        try {
            session = jsch.getSession(username, host, 22);
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword(password);
            session.connect();
            Channel channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp sftpChannel = (ChannelSftp) channel;
            InputStream is = sftpChannel.get(filePath);
            byte[] b = IOUtils.toByteArray(is);
            sftpChannel.exit();
            session.disconnect();
            return b;
//
//            int nRead;
//            byte[] data = new byte[16384];
//
//            while ((nRead = is.read(data, 0, data.length)) != -1) {
//              buffer.write(data, 0, nRead);
//            }
//
//            return buffer.toByteArray();
            
//            return IOUtils.toByteArray(is);
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            
        }
        return null;
    }
    
    public OperationResponse removeFtpFile(String fileFullPath) throws IOException {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        FTPClient client = new FTPClient();
        
        try {
            client.connect(host, port);
            if(!client.login(username, password)) {
                throw new Exception("Invalid ftp username/password");
            }
            
            if(client.deleteFile(fileFullPath)) {
                operationResponse.setData(fileFullPath);
                operationResponse.setCode(ResultCode.OK);
            }
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        finally {
            if(client.isConnected()) {
                client.logout();
                client.disconnect();
            }
        }
        
        return operationResponse;
    }
    
    public OperationResponse removeSFtpFile(String fileFullPath) throws IOException {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        JSch jsch = new JSch();
        Session session = null;
        
        try {
            session = jsch.getSession(username, host, 22);
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword(password);
            session.connect();
            Channel channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp sftpChannel = (ChannelSftp) channel;
            sftpChannel.rm(fileFullPath);
            sftpChannel.exit();
            session.disconnect();
            operationResponse.setCode(ResultCode.OK);
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
}
