package com.flip.service;

import com.flip.enums.ApplicationConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.io.IOUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;



@Service
public class VideoStreamService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Prepare the content.
     *
     * @param fileName String.
     * @param fileType String.
     * @param range    String.
     * @return ResponseEntity.
     */
    public ResponseEntity<byte[]> prepareContent(String fileType, String range, long fileSize, String path) throws Exception {
        long rangeStart = 0;
        long rangeEnd;
        byte[] data;
        try {
            if (range == null) {
                return ResponseEntity.status(HttpStatus.OK)
                        .header(ApplicationConstants.CONTENT_TYPE, fileType)
                        .header(ApplicationConstants.CONTENT_LENGTH, String.valueOf(fileSize))
                        .body(readByteRange(rangeStart, fileSize - 1, path)); // Read the object and convert it as bytes
            }
            String[] ranges = range.split("-");
            rangeStart = Long.parseLong(ranges[0].substring(6));
//            rangeStart = Long.parseLong(ranges[0]);
            if (ranges.length > 1) {
                rangeEnd = Long.parseLong(ranges[1]);
            } else {
                rangeEnd = fileSize - 1;
            }
            if (fileSize < rangeEnd) {
                rangeEnd = fileSize - 1;
            }
            data = readByteRange(rangeStart, rangeEnd, path);
        } catch (IOException e) {
            logger.error("Exception while reading the file {}", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        String contentLength = String.valueOf((rangeEnd - rangeStart) + 1);
        return ResponseEntity.status(HttpStatus.PARTIAL_CONTENT)
                .header(ApplicationConstants.CONTENT_TYPE, fileType)
                .header(ApplicationConstants.ACCEPT_RANGES, ApplicationConstants.BYTES)
                .header(ApplicationConstants.CONTENT_LENGTH, contentLength)
                .header(ApplicationConstants.CONTENT_RANGE, ApplicationConstants.BYTES + " " + rangeStart + "-" + rangeEnd + "/" + fileSize)
                .body(data);
    }
    
    
    public byte[] readByteRange(long start, long end, String path) throws Exception {
        FTPClient client = new FTPClient();
        try {
            
            client.connect("172.16.208.107", 21);
            if(!client.login("emsysftp", "KJg@!#S")) {
                throw new Exception("Invalid ftp username/password");
            }
            client.enterLocalPassiveMode();
            client.setFileType(FTP.BINARY_FILE_TYPE);
            try(InputStream inputStream = client.retrieveFileStream(path)) {
                byte[] b = IOUtils.toByteArray(inputStream);
                int rangeSize = (int) (end - start) + 1;
                byte[] result = new byte[rangeSize];
                
                System.arraycopy(b, (int) start, result, 0, result.length);
                return result;
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
                
                    if(client.isConnected()) {
                        client.logout();
                        client.disconnect();
                    }
                }
        
//        try (ByteArrayOutputStream bufferedOutputStream = new ByteArrayOutputStream()) {
//            byte[] data = new byte[ApplicationConstants.BYTE_RANGE];
//            int nRead;
//            while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
//                bufferedOutputStream.write(data, 0, nRead);
//            }
//            bufferedOutputStream.flush();
            
//            System.out.println("bufferedOutputStream.toByteArray() before$$$$ " + bufferedOutputStream.toByteArray().length);
//            if(bufferedOutputStream.toByteArray().length < rangeSize) {
//                rangeSize = bufferedOutputStream.toByteArray().length;
//            }
            
//            System.out.println("bufferedOutputStream.toByteArray()$$$$ " + bufferedOutputStream.toByteArray().length);
//            System.out.println("result.length$$$$ " + result.length);
            
//        }
    return null;
    }
}