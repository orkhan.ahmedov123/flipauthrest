/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author otahmadov
 */
@Repository
public class KurentoDao {
    private static final String ADNSU_DB_URL = "jdbc:postgresql://172.16.212.59:5432/";
    private static final String ADNSU_DB_USER = "postgres";
    private static final String OTHER_DB_URL = "jdbc:postgresql://172.16.208.107:5432/";
    private static final String OTHER_DB_USER = "manager";
    public boolean changeBeginStatus(String topicId, String domain, String status) {
        String query = "update edutube_course_topic "
                     + " set begin_status = ? where id = ?";
        try(Connection connection = DriverManager.getConnection(getConnectionUrl(domain), getConnectionUsername(domain), "manager");
                PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setObject(1, status, java.sql.Types.OTHER);
                preparedStatement.setObject(2, topicId, java.sql.Types.OTHER);
                preparedStatement.executeUpdate();
                return true;
            
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
        
        return false;
    }
    
    public boolean changeDistantStatus(String meetingId, String domain, String status) {
        String query = "update course_meeting "
                     + " set distant_status = ? where id = ?";
        try(Connection connection = DriverManager.getConnection(getConnectionUrl(domain), getConnectionUsername(domain), "manager");
                PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setObject(1, status, java.sql.Types.OTHER);
                preparedStatement.setObject(2, meetingId, java.sql.Types.OTHER);
                preparedStatement.executeUpdate();
                return true;
            
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
        
        return false;
    }
    
    public boolean changeConferenceMeeting(String meetingId, String domain, String status) {
        String query = "update confrance_meeting "
                     + " set moderator_status = ? where id = ?";
        try(Connection connection = DriverManager.getConnection(getConnectionUrl(domain), getConnectionUsername(domain), "manager");
                PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setObject(1, status, java.sql.Types.OTHER);
                preparedStatement.setObject(2, meetingId, java.sql.Types.OTHER);
                preparedStatement.executeUpdate();
                return true;
            
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
        
        return false;
    }
    
    public boolean changeConferenceRoom(String meetingId, String domain, String status) {
        String query = "update confrance_room "
                     + " set moderator_status = ? where id = ?";
        try(Connection connection = DriverManager.getConnection(getConnectionUrl(domain), getConnectionUsername(domain), "manager");
                PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setObject(1, status, java.sql.Types.OTHER);
                preparedStatement.setObject(2, meetingId, java.sql.Types.OTHER);
                preparedStatement.executeUpdate();
                return true;
            
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
        
        return false;
    }
    
    public boolean changeConferenceMeetingParticipant(String meetingId, String userId, String domain, String durationStatus, String beginDate, String endDate) {
        String query = "update confrance_meeting_participant "
                     + " set duration_status = ? where user_id = ? and meeting_id = ?";
        if(!beginDate.trim().isEmpty()) {
            query = "update confrance_meeting_participant "
                     + " set duration = coalesce(duration, 0) + (SELECT (extract(epoch from (to_timestamp(?, 'dd/mm/yyyy hh24:mi') - to_timestamp(?, 'dd/mm/yyyy hh24:mi'))) / 60)::int) "
                    + " where user_id = ? and meeting_id = ?";
        }
        try(Connection connection = DriverManager.getConnection(getConnectionUrl(domain), getConnectionUsername(domain), "manager");
                PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                if(beginDate.trim().isEmpty()) {
                    preparedStatement.setObject(1, durationStatus, java.sql.Types.OTHER);
                    preparedStatement.setObject(2, userId, java.sql.Types.OTHER);
                    preparedStatement.setObject(3, meetingId, java.sql.Types.OTHER);
                }else {
                    preparedStatement.setObject(1, beginDate, java.sql.Types.OTHER);
                    preparedStatement.setObject(2, endDate, java.sql.Types.OTHER); 
                    preparedStatement.setObject(3, userId, java.sql.Types.OTHER);
                    preparedStatement.setObject(4, meetingId, java.sql.Types.OTHER);
                }
                    
               
                preparedStatement.executeUpdate();
                return true;
            
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
        
        return false;
    }
    
    public boolean changeConferenceRoomParticipant(String meetingId, String userId, String domain, String status) {
        String query = "update confrance_room_participant "
                     + " set status = ? where user_id = ? and confrance_room_id = ?";
        try(Connection connection = DriverManager.getConnection(getConnectionUrl(domain), getConnectionUsername(domain), "manager");
                PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                    preparedStatement.setObject(1, status, java.sql.Types.OTHER);
                    preparedStatement.setObject(2, userId, java.sql.Types.OTHER);
                    preparedStatement.setObject(3, meetingId, java.sql.Types.OTHER);
                    
               
                preparedStatement.executeUpdate();
                return true;
            
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
        
        return false;
    }
    
    public List<Map<String, String>> getConfranceMeetingAgendaList(String meetingId) {
        List<Map<String, String>> list = new ArrayList<>();
        String query = "select * from v_confrance_meeting_agenda where meeting_id = ?";
        try(Connection connection = DriverManager.getConnection("jdbc:postgresql://172.16.208.107:5432/empro_emsdb", "manager", "manager");
                PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                    preparedStatement.setObject(1, meetingId, java.sql.Types.OTHER);
                    try(ResultSet resultSet = preparedStatement.executeQuery()) {
                        while(resultSet.next()) {
                            String position = resultSet.getString("position") != null ? resultSet.getString("position") :"";
                            String workPlace = resultSet.getString("work_place") != null ? resultSet.getString("work_place") :"";
                            String contact = resultSet.getString("contact") != null ? resultSet.getString("contact") :"";
                            Map<String, String> map = new HashMap<>();
                            map.put("agendaId", resultSet.getString("id"));
                            map.put("userId", resultSet.getString("user_id"));
                            map.put("userFullname", resultSet.getString("user_fullname"));
                            map.put("startDateTime", resultSet.getString("start_date_time"));
                            map.put("duration", resultSet.getString("duration"));
                            map.put("title", resultSet.getString("title"));
                            map.put("workPlace", workPlace);
                            map.put("contact", contact);
                            map.put("position",  position);
                            map.put("status", "0");
                            map.put("startTime", "");
                            list.add(map);
                        }
                    }
            
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
        
        return list;
    }
    
    public List<Map<String, Object>> getConfranceMeetingPollList(String meetingId) {
        List<Map<String, Object>> list = new ArrayList<>();
        String query = "select * from v_confrance_meeting_poll where meeting_id = ?";
        try(Connection connection = DriverManager.getConnection("jdbc:postgresql://172.16.208.107:5432/empro_emsdb", "manager", "manager");
                PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                    preparedStatement.setObject(1, meetingId, java.sql.Types.OTHER);
                    try(ResultSet resultSet = preparedStatement.executeQuery()) {
                        while(resultSet.next()) {
                            Map<String, Object> map = new HashMap<>();
                            map.put("id", resultSet.getString("id"));
                            map.put("content", resultSet.getString("content"));
                            map.put("choices", getConfranceMeetingPollChoiceList(connection, resultSet.getString("id")));
                            map.put("status", "0");
                            list.add(map);
                        }
                    }
            
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
        
        return list;
    }
    
    
    private List<Map<String,String>> getConfranceMeetingPollChoiceList(Connection connection, String pollId) {
        List<Map<String,String>> list = new ArrayList<>();
        String query = "select id, choice from v_confrance_meeting_poll_choice where poll_content_id = ?";
        try(PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                    preparedStatement.setObject(1, pollId, java.sql.Types.OTHER);
                    try(ResultSet resultSet = preparedStatement.executeQuery()) {
                        while(resultSet.next()) {
                            String choice = resultSet.getString("choice");
                            Map<String, String> map = new HashMap<>();
                            map.put("id", resultSet.getString("id"));
                            map.put("choice", choice);
                            map.put("count", "0");
                            list.add(map);
                        }
                    }
            
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
        
        return list;
    }
    
    public boolean updateChoices(List<Map<String, String>> list, String pollId) {
        try(Connection connection = DriverManager.getConnection("jdbc:postgresql://172.16.208.107:5432/empro_emsdb", "manager", "manager")) {
                    for(Map<String, String> m: list) {
                        updateChoisesCount(connection, pollId, m.get("id"), m.get("count"));
                    }
            
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
        
        return false;
    }
    
    public boolean updateChoices(String pollId, String choiseId, String count) {
        try(Connection connection = DriverManager.getConnection("jdbc:postgresql://172.16.208.107:5432/empro_emsdb", "manager", "manager")) {
                    
                        updateChoisesCount(connection, pollId, choiseId, count);
                   
            
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
        
        return false;
    }
    
    private boolean updateChoisesCount(Connection connection, String pollId, String choiseId, String count) {
        String query = "update confrance_meeting_poll_choices " +
                        "	set count = ? " +
                        "	where id = ? and poll_content_id = ?";
        try(PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                    preparedStatement.setObject(1, count, java.sql.Types.OTHER);
                    preparedStatement.setObject(2, choiseId, java.sql.Types.OTHER);
                    preparedStatement.setObject(3, pollId, java.sql.Types.OTHER);
                    preparedStatement.executeUpdate();
                    return true;
            
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
        
        return false;
    }
    
    private String getConnectionUrl(String domain) {
        
        return domain.toLowerCase().equals("adnsu_emsdb") ? ADNSU_DB_URL + domain : OTHER_DB_URL + domain;
    }
    private String getConnectionUsername(String domain) {
        
        return domain.toLowerCase().equals("adnsu_emsdb") ? ADNSU_DB_USER : OTHER_DB_USER;
    }
}
