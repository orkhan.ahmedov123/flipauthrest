/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.dao;

import com.flip.db.DBConnect;
import com.flip.domain.FileWrapper;
import com.flip.domain.UserProperties;
import com.flip.domain.MultilanguageString;
import com.flip.domain.OperationResponse;
import com.flip.domain.PrivilegeUrl;
import com.flip.domain.User;
import com.flip.enums.ResultCode;
import com.flip.enums.SqlConstants;
import com.flip.form.CheckForm;
import com.flip.form.InfoForm;
import com.flip.form.OperationCodeForm;
import com.flip.form.RegisterForm;
import com.flip.util.Crypto;
import com.flip.util.IdGenerator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.apache.log4j.Logger;

/**
 *
 * @author ahmadov
 */
@Repository
public class AuthDao implements IAuthDao {
    private static final Logger log = Logger.getLogger(AuthDao.class);
    @Autowired
    private DBConnect dbConnect;
    
    @Override
    public OperationResponse checkToken(String token) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        String query = "select * from user_session  where token = ? and active = 1";
        try(Connection connection = dbConnect.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setObject(1, token, java.sql.Types.OTHER);
               
                try(ResultSet resultSet = preparedStatement.executeQuery()) {
                    if(resultSet.next()) {
                    operationResponse.setData(this.getUserDetails(resultSet.getString("user_id"), connection));
                    operationResponse.setCode(ResultCode.OK);
                    
                    }
                }
            
        } 
        catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    

    @Override
    public OperationResponse doLogin(UserProperties jwtUser) { 
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        long id = 0;
        try(Connection connection = dbConnect.getConnection()) {
            try(PreparedStatement preparedStatement = connection.prepareStatement(SqlConstants.DO_LOGIN_QUERY)) {
                preparedStatement.setString(1, jwtUser.getUsername().toLowerCase());
                preparedStatement.setString(2, Crypto.encodeBase64(jwtUser.getPassword()));
                try(ResultSet resultSet = preparedStatement.executeQuery()) {
                    if(resultSet.next()) {
                        operationResponse.setData(this.getUserDetails(resultSet.getString("id"), connection));
                        operationResponse.setCode(ResultCode.OK);
                    }
                    
                    
                }
                
            }
            
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    
    private User getUserDetails(String userId, Connection connection) {
        
            try(PreparedStatement preparedStatement = connection.prepareStatement(SqlConstants.USER_QUERY)) {
                preparedStatement.setObject(1, userId, java.sql.Types.OTHER);
                
                try(ResultSet resultSet = preparedStatement.executeQuery()) {
                    if(resultSet.next()) {
                        return new User(resultSet.getString("id"), 
                                        resultSet.getString("flip_coin"),
                                        resultSet.getString("email"),
                                        resultSet.getString("phone"), 
                                        resultSet.getString("user_type"), 
                                        resultSet.getString("register_type"), 
                                        resultSet.getString("photo_file_id"), 
                                        resultSet.getString("fullname"), 
                                        resultSet.getString("create_date"),
                                        this.getUrlList(connection));
                        }
                }
                
            } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }
    public String getUserCoin(String userId) {
        
            try(Connection connection = dbConnect.getConnection(); 
                    PreparedStatement preparedStatement = connection.prepareStatement(SqlConstants.USER_QUERY)) {
                preparedStatement.setObject(1, userId, java.sql.Types.OTHER);
                
                try(ResultSet resultSet = preparedStatement.executeQuery()) {
                    if(resultSet.next()) {
                        return resultSet.getString("flipCoin");
                    }
                }
                
            } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }
    private List<String> getUrlList(Connection connection) {
            List<String> list = new ArrayList<>();
            try(PreparedStatement preparedStatement = connection.prepareStatement(SqlConstants.GET_ALL_PRIVILEGE_QUERY)) {
                
                try(ResultSet resultSet = preparedStatement.executeQuery()) {
                    while(resultSet.next()) {
                        list.add(resultSet.getString("url"));
                    }
                }
                
            } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return list;
    }


    @Override
    public OperationResponse doExit(String token) { 
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        String query = "update user_session set active = 0 where token like '%'||?||'%'";
        try(Connection connection = dbConnect.getConnection()) {
            String userId = getUserIdByTokenInAppSession(connection, token);
            if(userId.trim().isEmpty()) {
                return operationResponse;
            }
            try(PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setString(1, token);
                preparedStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);
            }
            
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    private String getUserIdByTokenInAppSession(Connection connection, String token) {
        String query = "select user_id from user_session where active = 1 and token like '%'||?||'%'";
        try(PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, token);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()) {
                return resultSet.getString(1);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }
    
     
    @Override
     public OperationResponse changeUserInfo(InfoForm form, String id, String type) { 
         OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
         String query = "";
         boolean passStatus = false;
         if(type.equals("PASSWORD")) {
                if(form.getOldPassword().trim().isEmpty()) {
                    operationResponse.setCode(ResultCode.INVALID_PARAMS);
                    operationResponse.setMessage(new MultilanguageString("Köhnə şifrə boşdur!!!", 
                                                                         "Köhnə şifrə boşdur!!!", 
                                                                         "Köhnə şifrə boşdur!!!"));
                    return operationResponse;
                }
                if(!form.getNewPassword().equals(form.getConfirmNewPassword())) {
                    operationResponse.setCode(ResultCode.INVALID_PARAMS);
                    operationResponse.setMessage(new MultilanguageString("Yeni şifrə və təkrarı uyğun deyil!!!", 
                                                                         "Yeni şifrə və təkrarı uyğun deyil!!!", 
                                                                         "Yeni şifrə və təkrarı uyğun deyil!!!"));
                    return operationResponse;
                }
                if(form.getNewPassword().trim().length() < 8) {
                    operationResponse.setCode(ResultCode.INVALID_PARAMS);
                    operationResponse.setMessage(new MultilanguageString("Şifrə minimum 8 simvol olmalıdır!!!", 
                                                                         "Şifrə minimum 8 simvol olmalıdır!!!", 
                                                                         "Şifrə minimum 8 simvol olmalıdır!!!"));
                    return operationResponse;
                }
                query = "update users set password = ? where id = ?";
                passStatus = true;
         } else if(type.equals("PHOTO")) {
             query = "update users set photo_file_id = ? where id = ?";
         } else {
             query = "update users set photo_file_id = ?, phone = ?, fullname = ? where id = ?";
         }
         
        try(Connection connection = dbConnect.getConnection()) {
            if(passStatus) {
                
                String passResult = checkUserPassword(connection, id, form.getOldPassword());
                if(!passResult.equals("1")) {
                    operationResponse.setCode(ResultCode.INVALID_PARAMS);
                    operationResponse.setMessage(new MultilanguageString("Köhnə şifrə yanlışdır!!!", 
                                                                         "Köhnə şifrə yanlışdır!!!", 
                                                                         "Köhnə şifrə yanlışdır!!!"));
                    return operationResponse;
                }
            }
            
            try(PreparedStatement preparedStatement = connection.prepareStatement(query)) {
               
                if(type.equals("PASSWORD")) {
                    preparedStatement.setObject(1, Crypto.encodeBase64(form.getNewPassword()), java.sql.Types.OTHER);
                    preparedStatement.setObject(2, id, java.sql.Types.OTHER);
                } else if(type.equals("PHOTO")) {
                    preparedStatement.setObject(1, form.getPhotoFileId(), java.sql.Types.OTHER);
                    preparedStatement.setObject(2, id, java.sql.Types.OTHER);
                } else {
                    preparedStatement.setObject(1, form.getPhotoFileId(), java.sql.Types.OTHER);
                    preparedStatement.setObject(2, form.getPhone(), java.sql.Types.OTHER);
                    preparedStatement.setObject(3, form.getFullname(), java.sql.Types.OTHER);
                    preparedStatement.setObject(4, id, java.sql.Types.OTHER);
                }
                
                preparedStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);
               
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
     
     
     private String checkUserPassword(Connection connection, String userId, String pass) { 
         String query = "SELECT count(*) FROM users u WHERE id = ? AND password = ?  AND active = 1";
        try(PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setObject(1, userId, java.sql.Types.OTHER);
            preparedStatement.setObject(2, Crypto.encodeBase64(pass), java.sql.Types.OTHER);
            try(ResultSet rs = preparedStatement.executeQuery()) {
                if(rs.next()) return rs.getString(1);
            }
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "0";
    }
     
    @Override
    public OperationResponse addUserSession(long userId, String token) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try(Connection connection = dbConnect.getConnection()) {
            try(PreparedStatement preparedStatement = connection.prepareCall(SqlConstants.ADD_USER_SESSION)) {
                preparedStatement.setObject(1, IdGenerator.getId(), java.sql.Types.OTHER);
                preparedStatement.setObject(2, token, java.sql.Types.OTHER);
                preparedStatement.setObject(3, userId, java.sql.Types.OTHER);
                preparedStatement.setObject(4, "", java.sql.Types.OTHER);
                preparedStatement.setObject(5, "", java.sql.Types.OTHER);
                preparedStatement.setObject(6, userId, java.sql.Types.OTHER);
                preparedStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);
            }
        } 
        catch (Exception e) {
            log.error(e.getMessage(), e);
            
        }
        return operationResponse;
    }

    @Override
    public OperationResponse uploadFileAndGetId(String originalName, String contentType, String path, long fileSize, String userId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        String query = "insert into files(id, original_name, path, type, size, create_date, create_user_id, active) " +
                        " values(?,?,?,?,?,now(),?,1)";
        
        Date now = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yy");
        String date = simpleDateFormat.format(now);
        String id = IdGenerator.getIdType1();
        
        try(Connection connection = dbConnect.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query)) {

            preparedStatement.setObject(1, id, java.sql.Types.OTHER);
            preparedStatement.setObject(2, originalName, java.sql.Types.OTHER);
            preparedStatement.setObject(3, path, java.sql.Types.OTHER);
            preparedStatement.setObject(4, contentType, java.sql.Types.OTHER);
            preparedStatement.setObject(5, fileSize, java.sql.Types.OTHER);
            preparedStatement.setObject(6, userId, java.sql.Types.OTHER);
            preparedStatement.executeUpdate();
            operationResponse.setData(id);
            operationResponse.setCode(ResultCode.OK);
        } 
        catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    
    @Override
    public OperationResponse removeFile(String id, String userId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        String query = "update files set active = ?, update_date = now(), update_user_id = ? where id = ?";
        try(Connection connection = dbConnect.getConnection()) {
            if(!checkUserFile(connection, id, userId)) {
                return operationResponse;
            }
            try(PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setObject(1, 0, java.sql.Types.OTHER);
                preparedStatement.setObject(2, userId, java.sql.Types.OTHER);
                preparedStatement.setObject(3, id, java.sql.Types.OTHER);

                preparedStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);
            }
            
        } 
        catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    
    private boolean checkUserFile(Connection connection, String id, String userId) {
        String query = "select * from files where id = ? and create_user_id = ?";
        try(PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setObject(1, id, java.sql.Types.OTHER);
            preparedStatement.setObject(2, userId, java.sql.Types.OTHER);
            try(ResultSet rs = preparedStatement.executeQuery()) {
                if(rs.next()) return true;
            }
            
        } 
        catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return false;
    }
    
    @Override
    public OperationResponse getFileById(String id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        String query = "select * from files ff where ff.active = 1 and ff.id = ?"; 
       
        
        try(Connection connection = dbConnect.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setObject(1, id, java.sql.Types.OTHER);
            
            try(ResultSet resultSet = preparedStatement.executeQuery()) {
                if(resultSet.next()) {
                operationResponse.setData(new FileWrapper(resultSet.getString("id"), 
                                                          resultSet.getString("path"), 
                                                          resultSet.getString("original_name"),
                                                          resultSet.getString("type"),
                                                          resultSet.getString("size"),
                                                          resultSet.getString("create_user_id")));
                operationResponse.setCode(ResultCode.OK);
                
                
                }
            }
            
            
            
        } 
        catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @Override
    public OperationResponse getFileByIdAndCheckUser(String id, String userId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        String query = "select * from files ff where ff.active = 1 and ff.id = ? and ff.create_user_id = ?"; 
       
        
        try(Connection connection = dbConnect.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setObject(1, id, java.sql.Types.OTHER);
            preparedStatement.setObject(2, userId, java.sql.Types.OTHER);
            
            try(ResultSet resultSet = preparedStatement.executeQuery()) {
                if(resultSet.next()) {
                operationResponse.setData(new FileWrapper(resultSet.getString("id"), 
                                                          resultSet.getString("path"), 
                                                          resultSet.getString("original_name"),
                                                          resultSet.getString("type"),
                                                          resultSet.getString("size"),
                                                          resultSet.getString("create_user_id")));
                operationResponse.setCode(ResultCode.OK);
                
                
                }
            }
            
            
            
        } 
        catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public OperationResponse insertFbFile(String fileOriginalName, String filePath) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        String query = "insert into files(id, name, path, content_type, active) " +
                        " values(?,?,?,?,1)";
        
        String id = IdGenerator.getIdType1();
        
        try(Connection connection = dbConnect.getConnection()) {
            
           try(PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setObject(1, id, java.sql.Types.OTHER);
                preparedStatement.setObject(2, fileOriginalName, java.sql.Types.OTHER);
                preparedStatement.setObject(3, filePath, java.sql.Types.OTHER);
                preparedStatement.setObject(4, "image/png", java.sql.Types.OTHER);
                preparedStatement.executeUpdate();
                operationResponse.setData(id);
                operationResponse.setCode(ResultCode.OK);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    private String insertFbFileAndGetId(Connection connection, String fileOriginalName, String filePath) {
        String query = "insert into files(id, name, path, content_type, active) " +
                        " values(?,?,?,?,1)";
        
        String id = IdGenerator.getIdType1();
        
        try(PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setObject(1, id, java.sql.Types.OTHER);
                preparedStatement.setObject(2, fileOriginalName, java.sql.Types.OTHER);
                preparedStatement.setObject(3, filePath, java.sql.Types.OTHER);
                preparedStatement.setObject(4, "image/png", java.sql.Types.OTHER);
                preparedStatement.executeUpdate();
                return id;
           
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }
    
    
    @Override
    public OperationResponse registerUser(RegisterForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        String query = "insert into users(id, email, phone, password, fullname, user_type, register_type, confirm_status, create_date, active) " +
                        " values(?,?,?,?,?,?,?,?,now(),1)";
        
        String id = IdGenerator.getIdType1();
        try(Connection connection = dbConnect.getConnection()) {
           try(PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setObject(1, id, java.sql.Types.OTHER);
                preparedStatement.setObject(2, form.getEmail(), java.sql.Types.OTHER);
                preparedStatement.setObject(3, form.getPhone(), java.sql.Types.OTHER);
                preparedStatement.setObject(4,  Crypto.encodeBase64(form.getPassword()), java.sql.Types.OTHER);
                preparedStatement.setObject(5,  form.getFullname(), java.sql.Types.OTHER);
                preparedStatement.setObject(6,  "USER", java.sql.Types.OTHER);
                preparedStatement.setObject(7,  "0", java.sql.Types.OTHER);
                preparedStatement.setObject(8,  "1", java.sql.Types.OTHER);
                
                preparedStatement.executeUpdate();
                operationResponse.setData(id);
                operationResponse.setCode(ResultCode.OK);
//                EmailUtils utils = new EmailUtils();
//                utils.sendUserMail(form.getEmail(), code, EmailMessage.REGISTER_SUBJECT,
//                                                          EmailMessage.REGISTER_H1,
//                                                          EmailMessage.REGISTER_P);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    
    @Override
    public OperationResponse checkEmailOrPhone(CheckForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        String query = "";
        if(form.getType().equals("PHONE")) {
            if(form.getUserId().trim().isEmpty()) {
                query = "select u.id from users u where lower(u.phone) = ? and u.active = 1";
            } else {
                query = "select u.id from users u where lower(u.phone) = ? and u.id <> ? and  u.active = 1";
            }
        } else if(form.getType().equals("EMAIL")) {
            if(form.getUserId().trim().isEmpty()) {
                query = "select u.id from users u where lower(u.email) = ? and u.active = 1";
            } else {
                query = "select u.id from users u where lower(u.email) = ? and u.id <> ? and  u.active = 1";
            }
        } else {
            operationResponse.setCode(ResultCode.INVALID_PARAMS);
            return operationResponse;
        }
        
        try(Connection connection = dbConnect.getConnection()) {
        try(PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            if(form.getType().equals("PHONE")) {
                if(form.getUserId().trim().isEmpty()) {
                    preparedStatement.setObject(1, form.getPhone().toLowerCase(), java.sql.Types.OTHER);
                } else {
                    preparedStatement.setObject(1, form.getPhone().toLowerCase(), java.sql.Types.OTHER);
                    preparedStatement.setObject(2, form.getUserId(), java.sql.Types.OTHER);
                }
            } else if(form.getType().equals("EMAIL")) {
                if(form.getUserId().trim().isEmpty()) {
                    preparedStatement.setObject(1, form.getEmail().toLowerCase(), java.sql.Types.OTHER);
                } else {
                    preparedStatement.setObject(1, form.getEmail().toLowerCase(), java.sql.Types.OTHER);
                    preparedStatement.setObject(2, form.getUserId(), java.sql.Types.OTHER);
                }
            }
            
            try(ResultSet resultSet = preparedStatement.executeQuery()) {
                operationResponse.setCode(ResultCode.OK);
                if(resultSet.next()) {
                    operationResponse.setData(true);
                    
                } else {
                    operationResponse.setData(false);
                }
                return operationResponse;
            }
        } 
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @Override
    public OperationResponse checkAnnouncementOperationCode(OperationCodeForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        String query = "";
        if(form.getAnnouncementType().equals("RENT")) {
                query = "select * from rent where id = ? and operation_code = ? and active = 1";
        } else if(form.getAnnouncementType().equals("SALE")) {
              query = "select * from sale where id = ? and operation_code = ? and active = 1";      

        } else {
            operationResponse.setCode(ResultCode.INVALID_PARAMS);
            return operationResponse;
        }
        
        try(Connection connection = dbConnect.getConnection()) {
        try(PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setObject(1, form.getAnnouncementId() , java.sql.Types.OTHER);
            preparedStatement.setObject(2, form.getOperationCode(), java.sql.Types.OTHER);
            
            try(ResultSet resultSet = preparedStatement.executeQuery()) {
                operationResponse.setCode(ResultCode.OK);
                if(resultSet.next()) {
                    operationResponse.setData(true);
                    
                } else {
                    operationResponse.setData(false);
                }
                return operationResponse;
            }
        } 
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @Override
    public boolean checkUserDetailsByEmail(String email) {
        String query = "select * from users u where lower(u.email) = ? and u.active = 1";
        try(Connection connection = dbConnect.getConnection()) {
        try(PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setObject(1, email.toLowerCase(), java.sql.Types.OTHER);
            try(ResultSet resultSet = preparedStatement.executeQuery()) {
                if(resultSet.next()) {
                    return true;
                }
            }
        } 
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return false;
    }
    
    @Override
    public boolean checkEmailConfirmCode(String email, String code) {
        String query = "select * from users u where lower(u.email) = ? and u.active = 1 and u.confirm_email_code = ? and u.in_system = 0";
        try(Connection connection = dbConnect.getConnection()) {
        try(PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setObject(1, email.toLowerCase(), java.sql.Types.OTHER);
            preparedStatement.setObject(2, code, java.sql.Types.OTHER);
            try(ResultSet resultSet = preparedStatement.executeQuery()) {
                if(resultSet.next()) {
//                    return updateInSystemOrConfirmCode(resultSet.getString("id"), connection, "IN_SYSTEM", "");
                }
            }
        } 
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return false;
    }
    
    @Override
    public boolean updateConfirmCode(String email, String confirmCode) {
        String query = "update users set confirm_email_code = ? where lower(email) = lower(?) and active = 1";
        
        try(Connection connection = dbConnect.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            
                preparedStatement.setObject(1, confirmCode, java.sql.Types.OTHER);
                preparedStatement.setObject(2, email, java.sql.Types.OTHER);
            
            preparedStatement.executeUpdate();
            return true; 
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return false;
    }
    
    @Override
    public boolean checkConfirmCode(String email, String confirmCode) {
        String query = "select id from users where email = ? and confirm_email_code = ? and active = 1";
        
        try(Connection connection = dbConnect.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            
                preparedStatement.setObject(1, email, java.sql.Types.OTHER);
                preparedStatement.setObject(2, confirmCode, java.sql.Types.OTHER);
                try(ResultSet resultSet = preparedStatement.executeQuery()){
                    if(resultSet.next()) {
                        return true;
                    }
                }
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return false;
    }
    
    @Override
    public boolean updateNewPassword(String email, String password, String code) {
        String query = "update users set password = ? where lower(email) = lower(?) and active = 1 and confirm_email_code = ?";
        
        try(Connection connection = dbConnect.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            
                preparedStatement.setObject(1, Crypto.encodeBase64(password), java.sql.Types.OTHER);
                preparedStatement.setObject(2, email, java.sql.Types.OTHER);
                preparedStatement.setObject(3, code, java.sql.Types.OTHER);
            
            preparedStatement.executeUpdate();
            return true; 
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return false;
    }
    
    
}
