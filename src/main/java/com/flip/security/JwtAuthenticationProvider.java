/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.security;

import com.flip.domain.UserProperties;
import com.flip.enums.SecurityConstants;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

/**
 *
 * @author ahmadov
 */
@Component
public class JwtAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider{

    @Autowired
    private JwtValidator validator;

    @Autowired
    private HttpServletResponse response;
    
    @Override
    public boolean supports(Class<?> type) {
        return (JwtAuthenticationToken.class.isAssignableFrom(type));
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails ud, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {
        
    }

    @Override
    protected UserDetails retrieveUser(String string, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {
        JwtAuthenticationToken jwtAuthenticationToken = (JwtAuthenticationToken) usernamePasswordAuthenticationToken;
        String token = jwtAuthenticationToken.getToken();
        
        if(token.equals(SecurityConstants.OPTION_REQUEST)) {
            return new JwtUserDetails("option", 0L, token, null);
        }
        UserProperties jwtUser =  validator.validate(token);
        
        if(jwtUser == null) {
            try{
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
            } catch(Exception e) {
               e.printStackTrace();
            }
            
            throw new RuntimeException("JWT token is incorrect");
        }
        
        List<GrantedAuthority> grantedAuthoritys = AuthorityUtils       
                .commaSeparatedStringToAuthorityList(jwtUser.getRole());
        return new JwtUserDetails(jwtUser.getUsername(), jwtUser.getId(), token, grantedAuthoritys);
    }
    
}
