/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.security;

import com.flip.domain.UserProperties;
import static com.flip.enums.SecurityConstants.SECRET;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.stereotype.Component;

/**
 *
 * @author ahmadov
 */
@Component
public class JwtValidator {
    
    public UserProperties validate(String token) {
       UserProperties jwtUser = null;
        try {
             
            Claims body = Jwts.parser()
                    .setSigningKey(SECRET)
                    .parseClaimsJws(token)
                    .getBody();

            jwtUser = new UserProperties();

            jwtUser.setUsername(body.getSubject());
            jwtUser.setId(Long.parseLong((String) body.get("userId")));
            jwtUser.setRole((String) body.get("role"));
        } catch (Exception e) {
//            System.out.println(e);
        }
        return jwtUser;
    }
}
