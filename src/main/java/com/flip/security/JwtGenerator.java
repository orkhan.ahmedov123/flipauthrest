/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.security;

import com.flip.domain.UserProperties;
import static com.flip.enums.SecurityConstants.EXPIRATION_TIME;
import static com.flip.enums.SecurityConstants.SECRET;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import org.springframework.stereotype.Component;

/**
 *
 * @author ahmadov
 */
@Component
public class JwtGenerator {
    
    public String generate(UserProperties jwtuser) {
        ZonedDateTime zonedDateTime = ZonedDateTime.now(ZoneOffset.UTC).plus(EXPIRATION_TIME, ChronoUnit.MILLIS);
        
        Claims claims = Jwts.claims()
                .setSubject(jwtuser.getUsername())
                .setExpiration(Date.from(zonedDateTime.toInstant()));
        
        claims.put("userId", String.valueOf(jwtuser.getId()));
        claims.put("role", String.valueOf(jwtuser.getRole()));
        
        String token = Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
        
        
        return token;
        
    }
    
}
