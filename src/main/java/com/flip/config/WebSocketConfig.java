/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/**
 *
 * @author ahmadov
 */
@Configuration
@EnableWebSocketMessageBroker
//public class WebSocketConfig {
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer{

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/topic");
        config.setApplicationDestinationPrefixes("/app");
    }
    
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
            registry.addEndpoint("/chat").setAllowedOrigins("*");
            registry.addEndpoint("/chat").setAllowedOrigins("*").withSockJS();
//        registry.addEndpoint("/chat").setAllowedOrigins("*").setHandshakeHandler(new DefaultHandshakeHandler(new TomcatRequestUpgradeStrategy())).withSockJS();
    }
    
}


//@EnableWebSocket
//public class WebSocketConfig implements WebSocketConfigurer{
//
////    @Override
////    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
////       registry.addHandler(new WebSocketHandler(), "/socket").setAllowedOrigins("*");
////    }
////    @Bean
////    public ServletServerContainerFactoryBean createWebSocketContainer() {
////        ServletServerContainerFactoryBean container = new ServletServerContainerFactoryBean();
////        container.setMaxBinaryMessageBufferSize(1024000);
////        return container;
////    }
//}