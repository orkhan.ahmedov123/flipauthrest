/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.config;

import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.core.env.Environment;
import org.springframework.jmx.support.RegistrationPolicy;

/**
 *
 * @author ahmadov
 */
@Configuration
@EnableMBeanExport(registration=RegistrationPolicy.IGNORE_EXISTING)
public class DBConfig {
    
    @Autowired
    private Environment env;
    
    private static Map<String, String> configMap = new HashMap<>();
    
    
    @Bean
    public DataSource dataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
            dataSourceBuilder.url(env.getProperty("jdbc.url"));
            dataSourceBuilder.username(env.getProperty("jdbc.username"));
            dataSourceBuilder.password(env.getProperty("jdbc.password"));
            return dataSourceBuilder.build();   
    }
}
