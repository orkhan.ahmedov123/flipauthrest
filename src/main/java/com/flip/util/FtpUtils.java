/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.util;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author ahmadov
 */
public class FtpUtils {
    public static final Map<String, String> EXTENSIONS = new HashMap<>();
    public static final Map<String, String> TYPES = new HashMap<>();
    
    static {
        EXTENSIONS.put("application/pdf", ".pdf");
        EXTENSIONS.put("image/jpeg", ".jpg");
        EXTENSIONS.put("image/png", ".png");
        EXTENSIONS.put("application/msword", ".doc");
        EXTENSIONS.put("application/vnd.openxmlformats-officedocument.wordprocessingml.document", ".docx");
        EXTENSIONS.put("application/vnd.ms-excel", ".xls");
        EXTENSIONS.put("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", ".xlsx");
        EXTENSIONS.put("application/vnd.ms-powerpoint", ".ppt");
        EXTENSIONS.put("application/vnd.openxmlformats-officedocument.presentationml.presentation", ".pptx");
        EXTENSIONS.put("text/plain", ".txt");
        EXTENSIONS.put("video/mp4", ".mp4");
        EXTENSIONS.put("video/x-flv", ".flv");
        EXTENSIONS.put("application/x-mpegURL", ".m3u8");
        EXTENSIONS.put("video/MP2T", ".ts");
        EXTENSIONS.put("video/3gpp", ".3gp");
        EXTENSIONS.put("video/quicktime", ".mov");
        EXTENSIONS.put("video/x-msvideo", ".avi");
        EXTENSIONS.put("video/x-ms-wmv", ".wmv");
        EXTENSIONS.put("application/zip", ".zip");
        EXTENSIONS.put("video/webm", ".webm");
        EXTENSIONS.put("audio/webm", ".weba");
        EXTENSIONS.put("audio/wav", ".wav");
        EXTENSIONS.put("image/svg+xml", ".svg");
        EXTENSIONS.put("application/vnd.rar", ".rar");
        EXTENSIONS.put("audio/opus", ".opus");
        EXTENSIONS.put("video/ogg", ".ogv");
        EXTENSIONS.put("audio/ogg", ".oga");
        EXTENSIONS.put("video/mpeg", ".mpeg");
        EXTENSIONS.put("audio/mpeg", ".mp3");
        EXTENSIONS.put("image/gif", ".gif");
        EXTENSIONS.put("application/epub+zip", ".epub");
        EXTENSIONS.put("text/csv", ".csv");
        EXTENSIONS.put("image/bmp", ".bmp");
        EXTENSIONS.put("audio/aac", ".aac");
        
        
        TYPES.put("pdf", "application/pdf");
        TYPES.put("jpg", "image/jpeg");
        TYPES.put("jpeg", "image/jpeg");
        TYPES.put("png", "image/png");
    }
    
    public static final String getExtension(String contentType) {
        return EXTENSIONS.get(contentType);
    }
    
    public static final String getType(String extension) {
        return TYPES.get(extension);
    }
}
