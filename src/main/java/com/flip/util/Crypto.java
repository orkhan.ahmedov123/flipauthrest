/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.util;

import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.util.UUID;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author ahmadov
 */
public class Crypto {
    
    static String AB = "123456789ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghklmnpqrstuvwxyz";
    static String NUM = "0123456789";
    static SecureRandom rnd = new SecureRandom();
    
    public static final String getGuid() {
        UUID uuid = UUID.randomUUID();
        String guid = uuid.toString().replaceAll("-", "");
        return guid;
    }
    
    public static final String getDoubleGuid() {
        UUID uuid1 = UUID.randomUUID();
        UUID uuid2 = UUID.randomUUID();
        String guid = uuid1.toString().replaceAll("-", "") + uuid2.toString().replaceAll("-", "");
        return guid;
    }
    
    public static final String encodeBase64(String data) throws UnsupportedEncodingException {
        byte[] bs = Base64.encodeBase64(data.getBytes());
        return new String(bs, "UTF-8");
    }
    
    public static final String decodeBase64(String data) throws UnsupportedEncodingException {
        byte[] bs = Base64.decodeBase64(data.getBytes());
        return new String(bs, "UTF-8");
    }
    
    
    public static String randomString(int length) {
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }
        return sb.toString();
    }
    
    public static String randomNumber(int length) {
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            sb.append(NUM.charAt(rnd.nextInt(NUM.length())));
        }
        return sb.toString();
    }
}
