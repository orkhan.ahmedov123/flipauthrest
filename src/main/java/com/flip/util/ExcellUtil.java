/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.util;

/**
 *
 * @author ahmadov
 */
public class ExcellUtil {
    
    private final static String[] students = {"firstname","lastname","patronymic","genderId","pincode","birthdate",
                         "citizenshipId","maritalId","socialId","orphanId","militaryId","nationalityId",
                         "educationLineId","educationYearId","educationTypeId","educationLevelId","educationLangId",
                        "specialityId","specializationId","educationPaymentTypeId","score","cardNumber"};
    private final static  String[] studentAddress = {"pincode","typeId","addressParentId","addressId","addressName"};
    private final static  String[] studentContact = {"pincode","typeId","contact"};
    
    private final static  String[] teachers = {"firstname","lastname","patronymic","genderId","pincode","birthdate",
                         "citizenshipId","maritalId","socialId","orphanId","militaryId","nationalityId",
                         "inActionId","inActionDate","outActionId","outActionDate","orgType",
                        "orgId","staffTypeId","positionId","contractTypeId","teaching","cardNumber"};//yazilmalidir
    
    private final static  String[] teacher_subject = {"teacher_pincode","subject_name"};//yazilmalidir
    private final static  String[] technical_base = {"id","name","volume","area"};//yazilmalidir
    private final static  String[] course = {"id","edu_plan_id","code","subject_name","subject_code","academic_group","ep_semester_id","semester_id","lang_id","edu_year_id","mhour","shour","lhour","course_work","evaluation_id"};//yazilmalidir
    private final static  String[] course_teacher = {"id","course_id","teacher_pincode","lesson_type_id"};//yazilmalidir
    private final static  String[] course_student = {"id","student_pincode","course_id"};//yazilmalidir
    private final static  String[] course_half_groups = {"id","course_id","lesson_type_id","half_group_type_id"};//yazilmalidir
    private final static  String[] course_half_group_students = {"id","course_id","half_group_id","teacher_pincode"};//yazilmalidir
    private final static  String[] course_meeting = {"id","course_id","teacher_pincode","lesson_type_id","lesson_date","clock_id"};//yazilmalidir
    private final static  String[] journal = {"course_id","meeting_date","clock_id","student_pincode","point"};//yazilmalidir
    
    
    private final static String[] GENDER = {"110000003","110000004"};
    private final static String[] MARITAL = {"110000009","110000010"};
    private final static String[] CITIZENSHIP = {"110000015","110000016"};
    private final static String[] NATIONALITY = {"110000013","110000014"};
    private final static String[] MILITARY = {"110000007","110000008"};
    private final static String[] SOCIAL = {"110000005","110000006"};
    private final static String[] ORPHAN = {"110000012","110000013"};
    
    private final static String[] EDU_LEVEL = {"110000068","110000069"};
    private final static String[] EDU_TYPE = {"110000070","110000071"};
    private final static String[] EDU_PAY = {"110000072","110000073"};
    private final static String[] EDU_LANG = {"110000065","110000066","110000067"};
    private final static String[] EDU_LINE = {"110000038"};
    
    private final static String[] CONTACT_TYPE = {"110000059","110000060","110000061"};
    private final static String[] ADDRESS_TYPE = {"110000062","110000063","110000064"};
    private final static String[] ADDRESS_PARENT = {"110000085","110000402"};
    private final static String[] ADDRESS_ID = {"110000086","110000087"};
    private final static String[] EDU_YEAR = {"200914524509407256","191220342802442401","9","2","3","4","201016570203354221"};
    
    private static final  String[] TEACHING = {"1","0"};
    private static final  String[] IN_ACTION = {"110000088","110000089"};
    private static final  String[] STAFF = {"110000105","110000106","110000233"};
    private static final  String[] CONTRACT_TYPE = {"110000092","110000093","110000094"};
    private static final  String[] OUT_ACTION = {"110000401","110000144"};
    private static final  String[] POSITION = {"110000220","110000219","110000221","110000223","110000222","110000224","110000225",
                                              "110000104","110000226","110000227","110000228","110000103","110000230",
                                                "110000229","110000193","210510035205637490","210510123907943623","210509580709104996"};
    private static final  String[] FACULTY = {"210517152106016112","210517134800177105"};
 
    private static final  String[] DEPARTMENT = {"201011173304306152"};
    private static final  String[] SPECIALITY_NAA = {"210512215900446449","210512244604363722","210512253304431583"
                                                   ,"210512255403608391","2105122637020610712","210512270408738234"
                                                   ,"210512280908489341","210512340408372476","210512452207162004"
                                                   ,"210516084203519599","2105160912033310089","210516095306608305"
                                                   ,"2105161118059610952","210514273808972953","210516124201475022"
                                                   ,"210516124504799942","210516130105462629","210516132109318619"
                                                   ,"210516134100092181","210516145506446243","2105161609018610844"
                                                   ,"210516171502006700","210516192404333506","210516194307622391"
                                                   ,"2105165202097110477","210610351701986368","2105113454083110206"
                                                    ,"210517340808582327","210509362609966572","210509365703087671"
                                                    ,"210509373203164188","210509380502412078","210509404403752708"
                                                    ,"210509412103802429","210509422503324839","210509425205576124"
                                                    ,"2105094549066810202","210512031902773969","2105120508069210131"
                                                    ,"210512053107434637","210512060107537832","210512062900078615"
                                                    ,"210512160000978001","210514255803074222","210514261604973339"
                                                    ,"210514263905855730","210514265301074848","210514271301182908"
                                                    ,"210514263905855730","210514265301074848","210514271301182908"
                                                    ,"210514263905855730","210514265301074848","210514271301182908"
                                                    ,"210514263905855730","210514265301074848","210514271301182908"
                                                    ,"210514263905855730","210514265301074848","210514271301182908"
                                                    ,"210514263905855730","210514265301074848","210514271301182908"
                                                    ,"210514263905855730","210514265301074848","210514271301182908"
                                                    ,"210710145404778179","210715502006995294","210710222304876213"};
private static final  String[] SPECIALIZATION_NAA = {
"210515565708805888","2105155713027210602","210515573000145168","210515574105396437","2105155753063410193",
"210515580506138349","210515581505923340","210515583707879232","210515585106327271","210515590103144040",
"2105162526001510292","210516254602102056","210516264504775393","210516265301279719","210516392008023320",
"210516420207092864","210516425707267932","210516434208766811","210516435300143144","210516444004364282",
"210516534606947376","2105094806036610436","210509482100557189","2105094857078210822","210509491009993162",
"210509504807499411","210509514707681432","210509523802211974","2105095327007810160","210509533802836855",
"210509551400505903","210512085300074903","210512090706851277","210512092209222501","2105121013038310243",
"2105121201070210922","210512121801917640","210512123708411536","210512130207205591","2105142859049910505",
"210514291007821360","210514301503049406","210514303001282414","210514310706001599","210514311907135809",
"210511305200953093","210511322204324536","210511320800749056","210511311201482718","210511310300839332",
"210511305200953093","210511322204324536","210511320800749056","210511311201482718","210511310300839332",
"210511305200953093","210511322204324536","210511320800749056","210511311201482718","210511310300839332",
"210511305200953093","210511322204324536","210511320800749056","210511311201482718","210511310300839332",
"210511305200953093","210511322204324536","210511320800749056","210511311201482718","210511310300839332",
"210511305200953093","210511322204324536","210511320800749056","210511311201482718","210511310300839332",
"210511305200953093","210511322204324536","210511320800749056","210511311201482718","210511310300839332",
"210511305200953093","210511322204324536","210710175203385848","210710173404149478","210710170405646316",
"210710164805326983","210710163408118441","210710160301154879","210710150709431941","210710140400529762",
"210710132102888745","210710125900619360","210710105509066624","210710102703051020","210710101809144611",
"210710095307845170","210710093106258295","210710085908264028","210710083304602619","210710082304455821",
"210710080602664814","2107100727013010098","210711054006122162","210711092702679215","2107100154048110618",
"2107100144033910469","2107100134021810876","210710011908659329","210710003202081582","210710001404426380",
"210710000506364479","210709595702412318","210709594308773179","210710224209832730"};

    public static String getColumnNameByFileName(String fileName, int index) {
        
        if(fileName.equals("students")) {
            return students[index];
        }
        if(fileName.equals("studentAddress") || fileName.equals("teacherAddress")) {
            return studentAddress[index];
        }
        if(fileName.equals("studentContact") || fileName.equals("teacherContact")) {
            return studentContact[index];
        }
        if(fileName.equals("teachers")) {
            return teachers[index];
        }
        return "";
    }
    
    public static String getCellValueByKey(String key, String value, String fileName, String domain) {
        
        if(key.equals("genderId")) {
            String code = value.split("-")[0].substring(1);
            return GENDER[Integer.parseInt(code) - 1];
        }
        if(key.equals("citizenshipId")) {
            String code = value.split("-")[0].substring(1);
            return CITIZENSHIP[Integer.parseInt(code) - 1];
        }
        if(key.equals("maritalId")) {
            String code = value.split("-")[0].substring(1);
            return MARITAL[Integer.parseInt(code) - 1];
        }
        if(key.equals("socialId")) {
            String code = value.split("-")[0].substring(1);
            if(code.equals("3")) return "";
            return SOCIAL[Integer.parseInt(code) - 1];
        }
        if(key.equals("orphanId")) {
            String code = value.split("-")[0].substring(1);
            if(code.equals("3")) return "";
            return ORPHAN[Integer.parseInt(code) - 1];
        }
        if(key.equals("militaryId")) {
            String code = value.split("-")[0].substring(1);
            return MILITARY[Integer.parseInt(code) - 1];
        }
        if(key.equals("nationalityId")) {
            String code = value.split("-")[0].substring(1);
            return NATIONALITY[Integer.parseInt(code) - 1];
        }
        if(key.equals("educationLineId")) {
            return EDU_LINE[0];
        }
        if(key.equals("educationYearId")) {//bu hemise deyisilecek
            int code = 2020 - Integer.parseInt(value.split("\\/")[0]);
            return EDU_YEAR[code];
        }
        if(key.equals("educationTypeId")) {
            String code = value.split("-")[0].substring(1);
            return EDU_TYPE[Integer.parseInt(code) - 1];
        }
        if(key.equals("educationLevelId")) {
            String code = value.split("_")[1].substring(1);
            return EDU_LEVEL[Integer.parseInt(code) - 1];
        }
        if(key.equals("educationLangId")) {
            String code = value.split("-")[0].substring(1);
            return EDU_LANG[Integer.parseInt(code) - 1];
        }
        if(key.equals("specialityId")) {
            String code = value.split("-")[0].substring(1);
            if(String.valueOf(code.charAt(0)).equals("0")) {
                code = code.substring(1);
            }
            return SPECIALITY_NAA[Integer.parseInt(code) - 1];
        }
        if(key.equals("specializationId")) {
            String code = value.split("-")[0].substring(2);
            if(String.valueOf(code.charAt(0)).equals("0")) {
                code = code.substring(1);
            }
            
            return SPECIALIZATION_NAA[Integer.parseInt(code) - 1];
        }
        if(key.equals("educationPaymentTypeId")) {
            String code = value.split("-")[0].substring(1);
            return EDU_PAY[Integer.parseInt(code) - 1];
        }
        if((fileName.equals("studentContact") || fileName.equals("teacherContact"))&& key.equals("typeId")) {
            String code = value.split("-")[0].substring(1);
            return CONTACT_TYPE[Integer.parseInt(code) - 1];
        }
        if((fileName.equals("studentAddress") || fileName.equals("teacherAddress"))&& key.equals("typeId")) {
            String code = value.split("-")[0].substring(1);
            return ADDRESS_TYPE[Integer.parseInt(code) - 1];
        }
        if((fileName.equals("studentAddress") || fileName.equals("teacherAddress"))&& key.equals("addressId")) {
            String code = value.split("-")[0].substring(1);
            return ADDRESS_ID[Integer.parseInt(code) - 1];
        }
        if((fileName.equals("studentAddress") || fileName.equals("teacherAddress"))&& key.equals("addressParentId")) {
            String code = value.split("_")[1].substring(1);
            return ADDRESS_PARENT[Integer.parseInt(code) - 1];
        }
        if(key.equals("inActionId")) {
            String code = value.split("-")[0].substring(1);
            return IN_ACTION[Integer.parseInt(code) - 1];
        }
        if(key.equals("outActionId")) {
            if(value.trim().isEmpty()) return "0";
            String code = value.split("-")[0].substring(1);
            return OUT_ACTION[Integer.parseInt(code) - 1];
        }
        if(key.equals("orgType")) {
            return "";
        }
        if(key.equals("orgId")) {
            String code = value.split("-")[0].substring(1);
            String type = value.split("-")[0].substring(0,1);
            if(type.equals("F"))
                return FACULTY[Integer.parseInt(code) - 1];
            else if(type.equals("K"))
                return DEPARTMENT[Integer.parseInt(code) - 1];
        }
        if(key.equals("staffTypeId")) {
            String code = value.split("-")[0].substring(1);
            return STAFF[Integer.parseInt(code) - 1];
        }
        if(key.equals("positionId")) {
            String code = value.split("-")[0];
            if(String.valueOf(code.charAt(0)).equals("0")) {
                code = code.substring(1);
            } 
            return POSITION[Integer.parseInt(code) - 1];
        }
        if(key.equals("contractTypeId")) {
            String code = value.split("-")[0].substring(1);
            return CONTRACT_TYPE[Integer.parseInt(code) - 1];
        }
        if(key.equals("teaching")) {
            String code = value.split("-")[0].substring(1);
            return TEACHING[Integer.parseInt(code) - 1];
        }
        
        
        return value;
    }

    public static String getTableNameBySheet(String i, String type) {
        if(type.equals("tableName"))
        switch(i) {
            case "1": return "a_teacher_subject";
            case "2": return "a_technical_base";
            case "3": return "a_course";
            case "4": return "a_course_teacher";
            case "5": return "a_course_student";
            case "6": return "a_course_half_groups";
            case "7": return "a_course_half_group_students";
            case "8": return "a_course_meeting";
            case "9": return "a_journal";
        }
        else {
           switch(i) {
                case "1": return "2";
                case "2": return "4";
                case "3": return "9";
                case "4": return "4";
                case "5": return "3";
                case "6": return "4";
                case "7": return "4";
                case "8": return "6";
                case "9": return "5";
            } 
        }
        return "";
    }

    public static String getColumnNameBySheet(String i, int index) {
        switch(i) {
            case "1": return teacher_subject[index];
            case "2": return technical_base[index];
            case "3": return course[index];
            case "4": return course_teacher[index];
            case "5": return course_student[index];
            case "6": return course_half_groups[index];
            case "7": return course_half_group_students[index];
            case "8": return course_meeting[index];
            case "9": return journal[index];
        }
        return "";
    }
    
}
