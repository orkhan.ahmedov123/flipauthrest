FROM maven:3.6-jdk-8 as build

COPY src /app/src/
COPY pom-prod.xml /app/pom.xml

WORKDIR /app

RUN mvn clean package


FROM openjdk:8-jre

COPY --from=build /app/target/*.jar /FlipAuthRest.jar


EXPOSE 8080

ENV PORT 8080

CMD ["java", "-jar", "/FlipAuthRest.jar"]

